<?php
session_start();
if(!isset($_SESSION["usuario"])||$_SESSION["rol"]!=1004)
{
header("location:index.php");
}
include "funciones.php";
?> 
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/bootstrap-reboot.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">        
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"> 
        <link rel="stylesheet" href="css/zerogrid.css" type="text/css" media="screen">
        <link rel="stylesheet" href="css/responsive.css" type="text/css" media="screen">  
        <script src="js/jquery.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <link href="css/select2.min.css" rel="stylesheet" />
        <script src="js/select2.min.js"></script> 
        <script src="js/menu.js" type="text/javascript"></script>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>DOCENTE</title>
    </head>

    <body>
             
        
        <div class="extra">
            <header>
                        <div class="container-fluid">
                            <nav class="navbar">
                                <a class="navbar-brand"  target="_blank" rel="noopener noreferrer" href="http://www.umss.edu.bo/">
                                <?php echo '<img height="600 width="600" src="data:image/png;base64,'.$data1[0].' "> ';?>
                                </a>
                                <a class="esconder"  target="_blank" rel="noopener noreferrer" href="http://fcyt.umss.edu.bo/">
                                <?php echo '<img height="120" width="140" src="data:image/png;base64,'.$data2[0].' "> ';?>
                                </a>
                            </nav>
                        </div>
            </header>

            <header>
                        <div class="row">
                        <div class="col-lg-3"><p class="h3 text-muted"><?php echo Ver_DatosIU($_SESSION["usuario"], $_SESSION["rol"]);?></p> </div>
                            <div class="col-lg-3"></div>
                                <div class="col-lg-3"></div>								
                                <div class="col-lg-3"> 
                                    <a class="navbar-brand" href="logout.php"><input type="submit" name="enviar" value="CERRAR SESION" class="btn btn-success btn-lg"></a>
                        </div>
                    </div>                             
            </header> 

            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-2 btn-group-vertical">
                        <ul>
                            <button type="button"  name="answer" onclick="showDiv()"  class="btn btn-dark btn-lg espacio1" id="mostrarDocente" value="<?php echo $array1149[0]; ?>">COMPLETAR REGISTRO</button>   
                            <button type="button"  name="answer" onclick="solicitudDiv()"  class="btn btn-dark btn-lg espacio1" id="mostrarNoti"
                              value="<?php echo $array01[0]; ?>">SOLICITUD <i class="material-icons" style="font-size:24px;color:red">add_alert</i></button>    
                            <button type="button"  name="answer" onclick="showDiv1()"  class="btn btn-dark btn-lg espacio1" id="mostrarDocente2" >NOTIFICACIONES</button>
                            <div class="btn-group espacio1">
                                <button type="button" class="btn btn-dark btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="mostrarDocente3">
                                    OPCIONES
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <button  class="dropdown-item" onclick="showDivD()"  class="btn btn-light">EDITAR MIS DATOS</button>
                                    <button  class="dropdown-item" onclick="abandono()"  class="btn btn-light">ABANDONO</button>
                                    <button  class="dropdown-item" onclick="listaEstudiantesT()"  class="btn btn-light">LISTA ESTUDIANTES</button>
                                   <form action="funciones.php" method="POST" enctype="multipart/form-data"> 	  
                                         <input type="hidden" class="list-group-item" name="rol" id="rol" value=<?php echo $_SESSION['rol']?>>                                 
                                         <button class="dropdown-item" type="button" name="answer" onclick="myFunction()"  class="btn btn-dark btn-lg espacio1">REGLAMENTO</button>    
                                    </form>                   
                                </div>  
                            </div>                                                 
                        </ul> 
                    </div>
        
                    <div class="wrapper" id="registroperfilDiv" class="container-fluid" style="display:none;" class="answer_list">
                        <form action="funciones.php" method="POST" enctype="multipart/form-data"> 
                            <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>>  
                            
                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-sm-2 col-form-label">TITULO</label>
                                <div class="col-sm-10">
                                    <select class="form-control"  name="titulo">
                                        <?php while ($row = pg_fetch_array($titulo)):;?>                                                         
                                            <option><?php echo $row[0];?></option>                                                         
                                        <?php endwhile; ?>      
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-sm-2 col-form-label">CARRERA</label>
                                <div class="col-sm-10">
                                    <select  class="js-example-basic-single" style="width: 100%" name="carrera">
                                        <?php while ($row = pg_fetch_array($carrera)):;?>                                                         
                                            <option><?php echo $row[1];?></option>                                                         
                                        <?php endwhile; ?>      
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-sm-2 col-form-label">AREA</label>
                                <div class="col-10">
                                    <select class="js-example-basic-single" style="width: 100%" name="area">
                                        <?php while ($row = pg_fetch_array($area)):;?>                                                         
                                            <option><?php echo $row[1];?></option>                                                         
                                        <?php endwhile; ?>      
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-sm-2 col-form-label">EMAIL</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control"  name="email" aria-describedby="emailHelp" placeholder="Ingresar Email">                                    
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-sm-2 col-form-label">CELULAR</label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="tel" name="telefono"  placeholder="Ingresar Celular">                               
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-sm-2 col-form-label">LUGAR TRABAJO</label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" name="trabajo"  placeholder="Entel">                               
                                </div>
                            </div>
        
                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-sm-2 col-form-label">EXPERIENCIA</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" row="5" name="experiencia" placeholder="3 años"></textarea>
                                    <!-- <input class="form-control" type="text" name="experiencia"  placeholder="">   -->                      
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-sm-2 col-form-label">TIPO</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="tipo">
                                        <option>INTERNO</option>
                                        <option>INVITADO</option>                              
                                    </select>
                                </div>
                            </div>
                    
                            <div class="form-group row">
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-info" name="completarRegisDoc">ENVIAR</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="wrappertable" id="welcomeDiv" class="container-fluid" style="display:none;" class="answer_list">
                        <form action="funciones.php" method="POST" enctype="multipart/form-data"> 
                        <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>> 
                        <input type="hidden" class="list-group-item" name="rol" id="rol" value=<?php echo $_SESSION['rol']?>>                        
                        <div class="form-group row">
                                 <div class="col-11"></div>
                                     <div class="col-1">
                                      <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#mensajePDF">?</button>
                                 </div>
                            </div>          
                            
                        <div class="form-group row">
                            <label  class="col-2 col-form-label">BUSCAR</label>
                            <div class="col-sm-10">
                                <select class="js-example-basic-single" style="width: 100%" name="showPDF">
                                    <?php while ($row = pg_fetch_array($res511)):;?>                                                         
                                        <option value=<?php echo "'$row[7]'";?>>
                                            <?php 
                                                echo $row[1]."&nbsp;&nbsp;&nbsp;" ; echo $row[3]."&nbsp;&nbsp;&nbsp;";
                                                echo $row[4]."&nbsp;&nbsp;&nbsp;";  echo $row[5]."&nbsp;&nbsp;&nbsp;"; echo $row[7]."&nbsp;&nbsp;&nbsp;";
                                            ?>
                                        </option>                                                         
                                    <?php endwhile; ?>														
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-10"></div>
                            <div class="col-2">                                                                               
                                 <button type="submit" class="btn btn-info" value=<?php echo $row['nombre_pdf_doc'];?> name="showPDF">PDF</button>
                            </div>
                        </div>
                        </form>
                        <h2 class="tituloespacio">CORRECCIONES PERFILES DE ESTUDIANTES</h2>                         
                            <div class="row">                               
                            <div class="col-12"></div>                                     
                                <div class="col-12">                               
                                    <div class="scrollable">                              
                                    <form action="funciones.php" method="POST" enctype="multipart/form-data"> 
                                         <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>> 
                                         <input type="hidden" class="list-group-item" name="rol" id="rol" value=<?php echo $_SESSION['rol']?>>                                  
                                            <table>
                                                <thead class="thead-dark">
                                                <?php while ($persona =pg_fetch_array($res110)):;?> 
                                                <tr class="titulotabla">
                                                        <th colspan="2" >Titulo</th>
                                                        <td colspan="10"><?php echo $persona['titulo_doc_b'];?></td>
                                                 </tr>
                                                    <tr>    
                                                    <tr  class="titulotabla">
                                                        <th class="text-center">ID</th>                                                        
                                                        <th class="text-center">CodSIS  </th>
                                                        <th class="text-center">Nombre  </th>
                                                        <th class="text-center">Apellido P. </th>
                                                        <th class="text-center">Apellido M. </th> 
                                                        <th class="text-center">Estado</th>                                                                                                                                                                                                                                                                                                     
                                                        <th class="text-center">PDF</th> 
                                                                                                                                                                            
                                                    </tr>
                                                </thead>                                     
                                                <tbody>                                                                                                                                                                      
                                                        <td class="text-center">   <?php  echo $persona['id_doc'];?></td>                                                          
                                                        <td class="text-center">   <?php  echo $persona['cod_sis'];?></td> 
                                                        <td class="text-center">   <?php  echo $persona['nombre'];?></td> 
                                                        <td class="text-center">   <?php  echo $persona['apellidop'];?></td> 
                                                        <td class="text-center">   <?php  echo $persona['apellidom'];?></td> 
                                                        <td class="text-center">   <?php ?></td>                                                                                                   
                                                        <td class="text-center">   
                                                              <button type="submit" class="btn btn-outline-info btn-sm botonAceptar" value="<?php  echo $persona['nombre_pdf_doc'];?>" name="showPDF">PDF</button>
                                                        </td>  
                                                                                                                                                                                                                                                    
                                                    </tr>
                                                   <?php endwhile;?>
                                                </tbody>                                                                            
                                            </table>                                                                                                                               
                                    </div>                                   
                                </div> 
                            </div>  
                        
                        </form>                             
                    </div>  
                  
                    <div class="wrapper" id="editardatos" class="container-fluid" style="display:none;" class="answer_list">
                        <form action="funciones.php" method="POST">
                            <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>> 
                            <input type="hidden" class="list-group-item" name="rol" id="rol" value=<?php echo $_SESSION['rol']?>> 
                            <h2 class="temporal" col>EL FORMULARIO ESTA EN CONSTRUCCION CON 90% DE AVANCE, SOLO FALTA LA DEBIDA ADAPTACION DE UNA TABLA EN LA BD ESPECIFICA PARA LOS ADMIN, DIRECTOR DE CARRERA Y SUPREMO</h2>  
                            <div class="form-group row">
                                <label for="formGroupExampleInput" class="col-4 col-form-label">NOMBRE</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="nombre" name="nombre"  value="<?php  echo $linea[3];?>">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-4 col-form-label">APELLIDO PATERNO</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="apellidop" name="apellidop"  value="<?php  echo $linea[4];?>">		
                                </div>
                            </div>
                
                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-4 col-form-label">APELLIDO MATERNO</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="apellidom" name="apellidom"  value="<?php  echo $linea[5];?>">
                                </div>
                            </div>
                    
                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-4 col-form-label">FECHA NACIMIENTO</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="fec_nac" name="fec_nac" value="<?php  echo $linea[6];?>">
                                </div>
                            </div>
                    
                            <div class="form-group row">
                                <label for="formGroupExampleInput" class="col-4 col-form-label">TELEFONO</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="telefono" name="telefono" value="<?php  echo $linea[7];?>">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="formGroupExampleInput" class="col-4 col-form-label">EMAIL</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="correo" name="correo" value="<?php  echo $linea[8];?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-3">
                                    <button type="submit" class="btn btn-info" name="env_est">ENVIAR</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="wrappertable" id="solicitud" class="container-fluid" style="display:none;" class="answer_list">                    
                        <form action="funciones.php" method="POST" enctype="multipart/form-data"> 
                        <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>> 
                        <input type="hidden" class="list-group-item" name="rol" id="rol" value=<?php echo $_SESSION['rol']?>> 
                        <h2 class="temporal" col>LOS PDFs SON VISIBLES DE FORMA NORMAL DE MANERA LOCAL EN XAMPP, CREEMOS QUE SE DEBE A EL FIREWALL, BUG O MOTIVOS DE SEGURIDAD QUE NO SON VISIBLES MEDIANTE LA WEB.</h2>  
        
                        <h2 class="tituloespacio">ESTUDIANTES SOLICITANDO A SU PERSONA COMO TUTOR</h2>                         
                            <div class="row">                               
                            <div class="col-12"></div>                                     
                                <div class="col-12">                               
                                    <div class="scrollable">                              
                                        <form>                                        
                                            <table>
                                                <thead class="thead-dark">
                                                    <tr>
                                                        <th class="text-center">ID  </th>                                                        
                                                        <th class="text-center">CodSIS  </th>
                                                        <th class="text-center">Nombre  </th>
                                                        <th class="text-center">Apellido P. </th>
                                                        <th class="text-center">Apellido M. </th>                                                                                                                                                                                                                                                                
                                                        <th class="text-center">PDF</th>
                                                        <th class="text-center">Aceptar</th>  
                                                       <!-- <th class="text-center">Rechazar</th>  -->                                                                   
                                                    </tr>
                                                </thead>
                                                <?php while ( $persona1 =pg_fetch_array($res1369)):;?>  
                                                <tbody>
                                                    <tr>                                                                                                                              
                                                        <td class="text-center">   <?php  echo $persona1['id'];?></td>                                                          
                                                        <td class="text-center">   <?php  echo $persona1['cod_sis'];?></td> 
                                                        <td class="text-center">   <?php  echo $persona1['nombre'];?></td> 
                                                        <td class="text-center">   <?php  echo $persona1['apellidop'];?></td> 
                                                        <td class="text-center">   <?php  echo $persona1['apellidom'];?></td>                                                                                                                                                                                                
                                                        <td class="text-center">   
                                                              <button type="submit" class="btn btn-outline-info btn-sm botonAceptar" value="<?php  echo $persona1['nombre_pdf'];?>" name="showPDF">PDF</button>
                                                        </td>   
                                                        <td class="text-center"> 
                                                             <button type="submit" class="btn btn-outline-primary btn-sm botonAceptar"  value="<?php  echo $persona1['ci'];?>" name="aceptarEstu">ACEPTAR</button>
                                                       </td> 
                                                       <td class="text-center">                                                 
                                                             <button type="submit" class="btn btn-outline-danger btn-sm botonAceptar"  value="<?php  echo $persona1['ci'];?>" name="errorAsignacion">ERROR ASIGNACION</button>
                                                       </td>                                                                                                                                                     
                                                    </tr><?php endwhile;?>
                                                </tbody>                                                                            
                                            </table>                                                                                                                               
                                    </div>                                   
                                </div> 
                            </div>  
                        </form>
                                                    
                    </div>  

                      <div class="wrappertable"  id="listaEstudiantesT" class="container-fluid" style="display:none;" class="list">
                      <form action="funciones.php" method="POST" enctype="multipart/form-data"> 
                        <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>>                             
                      <!--  <div class="form-group row">
                            <label  class="col-2 col-form-label">BUSCAR</label>
                            <div class="col-sm-10">
                                <select class="js-example-basic-single" style="width: 100%" name="estudiante" value=<?php echo $records1[2]?>>
                                    <?php while ($row = pg_fetch_array($records1)):;?>                                                         
                                        <option>
                                            <?php 
                                                echo $row[1]."&nbsp;&nbsp;&nbsp;" ; echo $row[2]."&nbsp;&nbsp;&nbsp;"; echo $row[3]."&nbsp;&nbsp;&nbsp;";
                                                echo $row[4]."&nbsp;&nbsp;&nbsp;";  echo $row[5]."&nbsp;&nbsp;&nbsp;";
                                            ?>
                                        </option>                                                         
                                    <?php endwhile; ?>														
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-7"></div>
                            <div class="col-5">                                                                               
                                <button onclick = "Mymensaje1()" type="submit" class="btn btn-info "  value="validar2" name="validar1">ACEPTAR</button>

                                <button onclick = "Mymensaje()" type="submit" class="btn btn-danger float-right  "  name="remover1">REMOVER</button>
            
                                <script>
                                    function Mymensaje()
                                    {
                                        alert("no se ingreso datos")
                                    }
                                    function Mymensaje1()
                                    {
                                        alert("no se ingreso datos")
                                    }
                                </script>            
                            </div>
                        </div>-->
                        <h2 class="tituloespacio">ESTUDIANTES A MI TUTORIA</h2>                         
                            <div class="row">                               
                            <div class="col-12"></div>                                     
                                <div class="col-12">                               
                                    <div class="scrollable">                              
                                        <form>                                        
                                            <table>
                                                <thead class="thead-dark">
                                                    <tr>
                                                        <th class="text-center">ID  </th>                                                        
                                                        <th class="text-center">CodSIS  </th>
                                                        <th class="text-center">Nombre  </th>
                                                        <th class="text-center">Apellido P. </th>
                                                        <th class="text-center">Apellido M. </th>                                                                                                                                                                                                                                                                
                                                        <th class="text-center">Carrera</th>
                                                                                                                           
                                                    </tr>
                                                </thead>
                                                <?php while ($persona =pg_fetch_array($res512)):;?>  
                                                <tbody>
                                                    <tr>                                                                                                                              
                                                        <td class="text-center">   <?php  echo $persona['id'];?></td>                                                          
                                                        <td class="text-center">   <?php  echo $persona['cod_sis'];?></td> 
                                                        <td class="text-center">   <?php  echo $persona['nombre'];?></td> 
                                                        <td class="text-center">   <?php  echo $persona['apellidop'];?></td> 
                                                        <td class="text-center">   <?php  echo $persona['apellidom'];?></td>  
                                                        <td class="text-center">   <?php  echo $persona['nom_carre'];?></td>                                                                                                                                                                                                 
                                                    </tr><?php endwhile;?>
                                                </tbody>                                                                            
                                            </table>                                                                                                                               
                                    </div>                                   
                                </div> 
                            </div>  
                        </form>
                        </form>                             
                    </div>  
                        

                    <div class="wrapper" id="abandono" class="container-fluid" style="display:none;" class="answer_list">
                        <form action="funciones.php" method="POST" enctype="multipart/form-data"> 
                            <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>>                             
                            
                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-sm-2 col-form-label">MOTIVO</label>
                                <div class="col-sm-10">
                                    <select class="form-control" id="exampleFormControlSelect1" name="motivo">
                                        <?php while ($row = pg_fetch_array($motivos)):;?>                                                         
                                            <option value=<?php echo $row[0];?>><?php echo $row[1];?></option>                                                         
                                        <?php endwhile; ?>      
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-sm-2 col-form-label">DESCRIPCION</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" row="5" name="descripcion"></textarea>
                                    <!-- <input class="form-control" type="text" name="experiencia"  placeholder="">   -->                      
                                </div>
                            </div>
                                    
                            <div class="form-group row">
                                <div class="col-sm-10">
                                    <button type="submit" name="RenDoc" class="btn btn-info">ENVIAR</button>
                                </div>
                            </div>
                        </form>
                    </div>   

                    
                                                <!-- Modal -->
                 <div class="modal fade" id="mensajePDF" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                         <div class="modal-dialog modal-dialog-centered" role="document">
                                  <div class="modal-content">
                                        <div class="modal-header">
                                              <h5 class="modal-title" id="exampleModalLongTitle">AYUDA</h5>
                                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                              <span aria-hidden="true">&times;</span>
                                                          </button>
                                                    </div>
                                         <div class="modal-body">
                                                            1. Permitir Popups, los navegadores generalmente<br>
                                                            preguntan si usted desea admitirlos en la parte superior<br>
                                                            derecha de su barra de direccion".
                                          </div>
                                                          
                                          </div>
                                     </div>
                             </div>          

                    

                    <div class="wrapper" id="cambioTema" class="container-fluid" style="display:none;" class="answer_list"> </div> 

                    <div Class="wrapper" id="eliminarSubArea" class="container-fluid" style="display:none;" class="answer_list"></div>

                    <div Class="wrapper" id="eliminarArea" class="container-fluid" style="display:none;" class="answer_list"></div>

                    <div Class="wrapper" id="subarea" class="container-fluid" style="display:none;" class="answer_list"></div>

                    <div Class="wrapper" id="area" class="container-fluid" style="display:none;" class="answer_list"></div>	
    
                    <div Class="wrapper" id="reg" class="container-fluid" style="display:none;" class="answer_list"></div>
    
                    <div class="wrapper" id="enviarreg" class="container-fluid" style="display:none;" class="answer_list">	</div>
    
                    <div class="wrapper" id="registroperfilDivEstu" class="container-fluid" style="display:none;" class="answer_list"></div>
    
                    <div Class="wrapper" id="verificarUsuario" class="container-fluid" style="display:none;" class="answer_list"></div>
    
                    <div class="wrappertable"  id="verificarDocente" class="container-fluid" style="display:none;" class="list"></div>
    
                    <div Class="wrapper" id="editarArea" class="container-fluid" style="display:none;" class="answer_list"></div>
    
                    <div Class="wrapper" id="editarSubArea" class="container-fluid" style="display:none;" class="answer_list"></div>  

                    <div class="wrappertable"  id="abandono" class="container-fluid" style="display:none;" class="list"></div>    

                    <div class="wrappertable"  id="cambioTemaAdmin" class="container-fluid" style="display:none;" class="list"></div>   

                    <div class="wrapper" id="cambioTutor" class="container-fluid" style="display:none;" class="answer_list"></div>    

                    <div class="wrappertable"  id="cambiotemaEstu" class="container-fluid" style="display:none;" class="list"></div>  
                    
                    <div class="wrappertable" id="estudiantesSistema" class="container-fluid" style="display:none;" class="answer_list"></div>

                    <div class="wrappertable" id="docentesSistema" class="container-fluid" style="display:none;" class="answer_list"></div>

                    <div class="wrappertable" id="adminSistema" class="container-fluid" style="display:none;" class="answer_list"></div>

                    <div class="wrappertable" id="exportar" class="container-fluid" style="display:none;" class="answer_list"></div> 

                      <div class="wrappertable" id="bitacoraB" class="container-fluid" style="display:none;" class="answer_list"></div> 

                    <div class="wrappertable" id="bitacoraA" class="container-fluid" style="display:none;" class="answer_list"></div>   

                    
                    <div class="wrappertable" id="listaAreaA" class="container-fluid" style="display:none;" class="answer_list"></div>

                    <div class="wrappertable" id="listaAreaB" class="container-fluid" style="display:none;" class="answer_list"></div>

                     <div class="wrapper" id="CambioTutorEstu" class="container-fluid" style="display:none;" class="answer_list"></div>
                </div>     
            </div> 
            <div class="footer esconder">
                 <p>Copyright © 2018 EKEKO S.A. All rights reserved.</p>
           </div>
        </div>
    </body>

    <script>  
     blink(); 
     myFunction21();
    </script>
</html>

		

