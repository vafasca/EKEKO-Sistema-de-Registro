<?php   
    include "funciones.php";    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">    
    <link rel="stylesheet" href="css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery.js" type="text/javascript"></script>
    <script src="js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="js/menu.js" type="text/javascript"></script>    
    <link rel="stylesheet" href="css/jquery-ui.min.css">      
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"> 
    <link rel="stylesheet" href="css/zerogrid.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/responsive.css" type="text/css" media="screen">  
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="js/datepicker-es.js" type="text/javascript"></script>
    
</head>

<body>
	<div class="extra1">
    <!--==============================header=================================--> 
    <header>
        <div class="container">
            <a class="navbar-brand" href="index.php">
                <input type="submit" name="back" value="INICIO" class="btn btn-outline-info espacio"> 
            </a>                  
            <div class="row">                            
            <div class="col"></div> 
            <div class="col"></div>                              
        </div> 
    </header>
    <div class="container">
            <div class="row">
			<div class="col-sm-5"></div> 
                <div class="col-6">                                                    
                    <input type="button"  name="answer" value="REGISTRO"  class="btn btn-dark btn-lg">  
             </div>
         </div>
      </div>

        <div class="container titulo3">
            <div class="row">
			<div class="col-3"></div> 
                <div class="col-6">                                                    
                  
                    <form method="POST" action="funciones.php">
						<div class="form-group">
							<div class="form-check form-check-inline">
							<label class="container1">ESTUDIANTE
								  <input type="radio" checked="checked" name="radio" value="1007">
								  <span class="checkmark"></span>
							</label>
							</div>
							<div class="form-check form-check-inline radioboton">
								<label class="container1">DOCENTE
								  <input type="radio" name="radio" value="1004">
								  <span class="checkmark"></span>
								</label>
							</div>
						</div>
							
                            <div class="form-group">
                                <label for="address">Codigo Sis</label>
                                <input type="address" class="form-control" id="sname" name="nombre" placeholder="Codigo Sis">
                            </div>
                            <div class="form-group">
                                <label for="name">Celula Identidad</label>
                                <input type="name" class="form-control" id="fname" name="nombre1" placeholder="Ingresar C.I.">
                            </div>
                            <div class="form-group">
                                <label for="name">Contraseña</label>
                                <input type="name" class="form-control" id="fname" name="nombrepass" placeholder="Ingresar su Contraseña">
                            </div>
                            <div class="form-group">
                                <label for="name">Nombres</label>
                                <input type="name" class="form-control" id="fname" name="nombre2" placeholder="Ingresar Nombres">
                            </div>
                            <div class="form-group">
                                <label for="name">Apellido Paterno</label>
                                <input type="name" class="form-control" id="fname" name="nombre3" placeholder="Apellido paterno">
                            </div>
                            <div class="form-group">
                                <label for="name">Apellido Materno</label>
                                <input type="name" class="form-control" id="fname" name="nombre4" placeholder="Apellido Materno">
                            </div>
                            <div class="form-group">
                                <label>Fecha Nacimiento</label>
                                <input type="datepicker" id="datepicker" class="form-control" name="nombre5" placeholder="dia/mes/año">
                            </div>
                         <!--   <div class="form-group">
                                      <label >SECRETO</label>
                                      <div class="col-13">
                                          <select class="form-control" name="secreto" >
                                          <?php                                                          
                                                    while ($row = pg_fetch_array($preguntaSecre)):;?>                                                         
                                                     <option value=<?php echo $row[0];?>><?php echo $row[1];?></option>                                                         
                                            <?php endwhile;
                                             ?>   
                                          </select>
                                      </div>
                                  </div>
                                  <div class="form-group">                               
                                <input type="name" class="form-control" id="fname" name="secreto6" placeholder="Respuesta Secreta">
                            </div>-->
                            <button type="submit" name="registroPrincipal" class="btn btn-primary">REGISTRAR</button>
                        </form>
                    </div>  
				<div class="col-3"></div>                  
                </div>
             
            </div>      
        </div> 
        
    </div>
    <div class="footer">
             <p>Copyright © 2018 EKEKO S.A. All rights reserved.</p>
        </div> 
    <script>  
   $( function() {
    $( "#datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true
    });
  } );
     </script> 
</body>

</html>
