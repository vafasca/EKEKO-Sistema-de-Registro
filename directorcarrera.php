﻿<?php
session_start();
if(!isset($_SESSION["usuario"])||$_SESSION["rol"]!=1002)
{
header("location:index.php");
}
include "funciones.php";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/bootstrap-reboot.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
        <link rel="stylesheet" href="css/zerogrid.css" type="text/css" media="screen">
        <link rel="stylesheet" href="css/responsive.css" type="text/css" media="screen">
        <script src="js/jquery.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>          
        <link rel="stylesheet" href="css/jquery-ui.min.css">     
        <script src="js/menu.js" type="text/javascript"></script>
        <script src="js/jquery.js" type="text/javascript"></script>
        <script src="js/jquery-ui.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
      
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="js/datepicker-es.js" type="text/javascript"></script>
        <link href="css/select2.min.css" rel="stylesheet">
        <script src="js/select2.min.js"></script>    
        <title>DIRECTOR CARRERA</title>
    </head>

    <body>
       

        <div class="extra">
            <header>
                <div class="container-fluid">
                    <nav class="navbar">
                        <a class="navbar-brand"   target="_blank" rel="noopener noreferrer"href="http://www.umss.edu.bo/">
                            <?php echo '<img height="300" width="300" src="data:image/png;base64,'.$data1[0].' "> ';?>
                        </a>
                        <a class="esconder"  target="_blank" rel="noopener noreferrer" href="http://fcyt.umss.edu.bo/">
                            <?php echo '<img height="100" width="120" src="data:image/png;base64,'.$data2[0].' "> ';?>
                        </a>
                    </nav>
                </div>
            </header>

            <header>
                <div class="row">
                    <div class="col-lg-3">
                        <p class="h3 text-muted"><?php echo Ver_DatosIU($_SESSION["usuario"], $_SESSION["rol"]);?></p> 
                    </div>
                    <div class="col-lg-3"></div>
                    <div class="col-lg-3"></div>								
                    <div class="col-lg-3"> 
                        <a class="navbar-brand" href="logout.php">
                            <input type="submit" name="enviar" value="CERRAR SESION" class="btn btn-success btn-lg">
                        </a>
                    </div>
                </div>                 
            </header> 

            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-2 btn-group-vertical">
                        <ul>
                        <button type="button"  name="answer"  onclick="showDivR()" class="btn btn-dark btn-lg espacio1" id="mostrar">COMPLETAR REGISTRO</button>
                           
                            <div class="btn-group espacio1">
                                <button type="button" class="btn btn-dark btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">NOTIFICACIONES</button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <button  class="dropdown-item"  type="button" name="answer" onclick="showDiv1()" class="btn btn-light">PERFIL</button>
                                    <button  class="dropdown-item"  type="button" name="answer" onclick="estudiantesSistema()" class="btn btn-light">Estudiantes</button>
                                    <button  class="dropdown-item"  type="button" name="answer" onclick="docentesSistema()" class="btn btn-light">Docentes</button>
                                </div>
                            </div>
                            <div class="btn-group espacio1">
                            <button type="button" class="btn btn-dark btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">LISTA AREA Y SUB-AREA</button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <button class="dropdown-item" type="button" name="answer" onclick="listaAreaA()" class="btn btn-light espacio1">LISTA AREA</button>  
                                    <button  class="dropdown-item"  type="button" name="answer" onclick="listaAreaB()" class="btn btn-light">LISTA SUB-AREA</button>                                    
                                </div>
                                </div>
                            <div class="btn-group espacio1">
                                    <button type="button" class="btn btn-dark btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">AREA Y SUB-AREA</button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <button  class="dropdown-item" type="button" name="answer" onclick="showDiv2()" class="btn btn-light">Agregar Area</button>  
                                        <button  class="dropdown-item" type="button" name="answer" onclick="showSubArea()" class="btn btn-light">Agregar Sub-area</button>                                 
                                        <button  class="dropdown-item" type="button" name="answer" onclick="editarArea()" class="btn btn-light">Editar Area</button> 
                                        <button  class="dropdown-item" type="button" name="answer" onclick="editarSubArea()" class="btn btn-light">Editar Sub-area</button>   
                                        <button  class="dropdown-item" type="button" name="answer" onclick="showDeleteArea()" class="btn btn-light">Eliminar Area</button> 
                                        <button  class="dropdown-item" type="button" name="answer" onclick="showDeleteSubArea()" class="btn btn-light">Eliminar Sub-area</button>                                   
                                    </div>
                                </div>                      
                            <div class="btn-group espacio1">
                                <button type="button" class="btn btn-dark btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">OPCIONES</button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <button class="dropdown-item" type="button" name="answer" onclick="showDiv()" class="btn btn-light">REGISTRAR</button>
                                    <button class="dropdown-item" type="button" name="answer" onclick="showDivD()" >EDITAR MIS DATOS</button>
                                    <form action="funciones.php" method="POST" enctype="multipart/form-data"> 	  
                                         <input type="hidden" class="list-group-item" name="rol" id="rol" value=<?php echo $_SESSION['rol']?>>                                 
                                         <button class="dropdown-item" type="button" name="answer" onclick="myFunction()"  class="btn btn-dark btn-lg espacio1">REGLAMENTO</button>    
                                    </form>                                 
                                    <button class="dropdown-item" type="button" name="answer" onclick="showDiv3()"></button> 
                                </div>
                            </div>
                        </ul>
                    </div>
                       

                        
                        <div class="wrapper" id="registroperfilDivEstu" class="container-fluid" style="display:none;" class="answer_list">
                        <form action="funciones.php" method="POST" enctype="multipart/form-data"> 
                        <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>>                             
                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-sm-2 col-form-label">CARRERA</label>
                                <div class="col-sm-10">
                                    <select class="form-control" id="exampleFormControlSelect1" name="carrera">
                                        <?php while ($row = pg_fetch_array($carrera)):;?>                                                         
                                            <option value=<?php echo $row[0];?>><?php echo $row[1];?></option>                                                         
                                        <?php endwhile; ?>      
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-sm-2 col-form-label">EMAIL</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="exampleInputEmail1" name="email" aria-describedby="emailHelp" placeholder="Ingresar Email">                        
                                        </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-sm-2 col-form-label">CELULAR</label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" id="example-tel-input" name="telefono"  placeholder="Ingresar celular">                               
                                </div>
                            </div>
                                    
                            <div class="form-group row">
                                <div class="col-sm-10">
                                    <button type="submit" name="manhatam" class="btn btn-info">ENVIAR</button>
                                </div>
                            </div>
                        </form>
                    </div> 
                    

                    <div class="wrapper" id="registroperfilDiv" class="container-fluid" style="display:none;" class="answer_list">
                        <form action="funciones.php" method="POST">
                            <!--<div class="form-group">
                                <div class="form-check form-check-inline">
                                    <label class="container1">
                                        ESTUDIANTE
                                        <input type="radio" checked="checked" name="radio">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            
                                <div class="form-check form-check-inline">
                                    <label class="container1">
                                        DOCENTE
                                        <input type="radio" name="radio">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>

                                <div class="form-check form-check-inline">
                                    <label class="container1">
                                        ADMINISTRATIVO
                                        <input type="radio" name="radio">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>-->
                            <div class="form-group row">
                                <label for="Codigo Sis" class="col-12 col-form-label">REGISTRO NUEVO ADMINISTRADOR</label>                               
                            </div>

                            <div class="form-group row">
                                <label for="Codigo Sis" class="col-4 col-form-label">Codigo Sis</label>
                                <div class="col-8">
                                    <input type="address" class="form-control" name="cod_sis" id="cod_sis" placeholder="Codigo Sis">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="Celula de Identidad" class="col-4 col-form-label">Ingresar C.I.</label>
                                <div class="col-8">
                                    <input type="address" class="form-control" name="ci" id="ci" placeholder="Ingresar C.I.">
                                </div>
                            </div>
                            <div class="form-group row">
                            <label for="Nombres" class="col-4 col-form-label">Contraseña</label>
                                <div class="col-8">
                                <input type="name" class="form-control" id="fname" name="########################" placeholder="Ingresar Contraseña">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Nombres" class="col-4 col-form-label">Nombres</label>
                                <div class="col-8">
                                    <input type="address" class="form-control" name="nombre" id="nombre" placeholder="Nombres">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="Apellido Paterno" class="col-4 col-form-label">Apellido Paterno</label>
                                <div class="col-8">
                                    <input type="address" class="form-control" name="apellidop" id="apellidop" placeholder="Apellido Paterno">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="Apellido Materno" class="col-4 col-form-label">Apellido Materno</label>
                                <div class="col-8">
                                    <input type="address" class="form-control" name="apellidom" id="apellidom" placeholder="Apellido Materno">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="Fecha Nacimiento" class="col-4 col-form-label">Fecha Nacimiento</label>
                                <div class="col-8">
                                <input type="datepicker" id="datepicker" class="form-control" name="nombre5" placeholder="dia/mes/año">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-4 col-form-label">Carrera</label>
                                <div class="col-8">
                                    <select class="form-control" id="exampleFormControlSelect1" name="carrera">
                                        <?php while ($row = pg_fetch_array($carrera)):;?>                                                         
                                            <option value=<?php echo $row[0];?>><?php echo $row[1];?></option>                                                         
                                        <?php endwhile; ?>      
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-4 col-form-label">Email</label>
                                <div class="col-8">
                                    <input type="email" class="form-control" id="exampleInputEmail1" name="email" aria-describedby="emailHelp" placeholder="Ingresar Email">                        
                                        </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-4 col-form-label">Celular</label>
                                <div class="col-8">
                                    <input class="form-control" type="text" id="example-tel-input" name="telefono"  placeholder="Ingresar celular">                               
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-3">
                                    <button type="submit" class="btn btn-info" name="reg_dir">REGISTRAR</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div Class="wrapper" id="editardatos" class="container-fluid" style="display:none;" class="answer_list">
                        <form action="funciones.php" method="POST">
                            <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>> 
                            <input type="hidden" class="list-group-item" name="rol" id="rol" value=<?php echo $_SESSION['rol']?>>
                            
                            <div class="form-group row">
                                <label for="formGroupExampleInput" class="col-4 col-form-label">NOMBRE</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="nombre" name="nombre"  value="<?php  echo $linea[3];?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-4 col-form-label">APELLIDO PATERNO</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="apellidop" name="apellidop"  value="<?php  echo $linea[4];?>">		
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-4 col-form-label">APELLIDO MATERNO</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="apellidom" name="apellidom"  value="<?php  echo $linea[5];?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-4 col-form-label">FECHA NACIMIENTO</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="fec_nac" name="fec_nac" value="<?php  echo $linea[6];?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="formGroupExampleInput" class="col-4 col-form-label">TELEFONO</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="telefono" name="telefono" value="<?php  echo $linea[7];?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="formGroupExampleInput" class="col-4 col-form-label">EMAIL</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="correo" name="correo" value="<?php  echo $linea[8];?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-3">
                                    <button type="submit" class="btn btn-info" name="env_est">ENVIAR</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div Class="wrapper" id="editarArea" class="container-fluid" style="display:none;" class="answer_list">
                        <form action="funciones.php" method="POST">
                            <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>> 
                            
                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-3 col-form-label">AREA</label>
                                <div class="col-9">
                                    <select class="js-example-basic-single" style="width: 100%" name="area">
                                        <?php while ($row = pg_fetch_array($area)):;?>                                                        
                                            <option value=<?php echo $row['id_area'];?> onclick="tocarLista('<?php echo $row['nom_area'];?>', ' <?php $valor=$row['descripcion_area'];
                                                if($valor === '\'')
                                                {

                                                }
                                                else
                                                {
                                                    echo $valor;
                                                }?>')" ><?php echo $row['nom_area'];?>
                                            </option>                                                        
                                        <?php endwhile; ?>      
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-4 col-form-label">NOMBRE</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="nombreAreaEdit" name="nombre"  placeholder="<?php  ?>">		
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-4 col-form-label">DESCRIPCION</label>
                                <div class="col-8">
                                    <textarea class="form-control" rows="3" name="descripcion" id="descripAreaEdit"></textarea>	
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-3">
                                    <button type="submit" class="btn btn-info" name="editarArea">ACTUALIZAR</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div Class="wrapper" id="editarSubArea" class="container-fluid" style="display:none;" class="answer_list">
                        <form action="funciones.php" method="POST">
                            <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>> 
                            
                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-3 col-form-label">SUB-AREA</label>
                                <div class="col-9">
                                    <select class="js-example-basic-single" style="width: 100%"  name="subarea">
                                        <?php while ($row = pg_fetch_array($subarea)):;?>                                                         
                                            <option value=<?php echo $row['id_sub'];?> onclick="tocarListaSub('<?php echo $row['nom_sub'];?>', ' <?php $valor=$row['descripcion'];
                                                if($valor === '\'')
                                                {
                                                 
                                               }
                                                else
                                                {
                                                    echo $valor;
                                                }?>')" ><?php echo $row['nom_sub'];?>
                                            </option>                                                         
                                        <?php endwhile; ?>      
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-4 col-form-label">NOMBRE</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="nomSub" name="nombreSubArea"  placeholder="<?php  ?>">		
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-4 col-form-label">DESCRIPCION</label>
                                    <div class="col-8">
                                        <textarea class="form-control" rows="3" id="desSub" name="desSubArea"  placeholder="<?php  ?>">		</textarea>
                                    </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-3">
                                    <button type="submit" class="btn btn-info" name="editarSubArea">ACTUALIZAR</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div Class="wrapper" id="eliminarSubArea" class="container-fluid" style="display:none;" class="answer_list">
                        <form action="funciones.php" method="POST">
                            <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>> 
                            
                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-3 col-form-label">SUB-AREA</label>
                                <div class="col-9">
                                    <select class="js-example-basic-single" style="width: 100%"  name="subarea">
                                        <?php while ($row = pg_fetch_array($subareaEliminacion)):;?>                                                         
                                            <option value=<?php echo $row['id_sub'];?>><?php echo $row['nom_sub'];?></option>                                                          
                                        <?php endwhile; ?>      
                                    </select>
                                </div>
                            </div>
                            
                                </br>
                            <div class="form-group row">
                                <div class="col-3">
                                    <button type="submit" class="btn btn-info" name="eliminarSubArea">ELIMINAR</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div Class="wrapper" id="eliminarArea" class="container-fluid" style="display:none;" class="answer_list">
                        <form action="funciones.php" method="POST">
                            <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>> 
                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-3 col-form-label">AREA</label>
                                <div class="col-9">
                                    <select class="js-example-basic-single" style="width: 100%"  name="area">
                                        <?php while ($row = pg_fetch_array($areaEliminacion)):;?>                                                         
                                            <option value=<?php echo $row['id_area'];?>><?php echo $row['nom_area'];?></option>                                                          
                                        <?php endwhile; ?>      
                                    </select>
                                </div>
                            </div>
                            
                                </br>
                            <div class="form-group row">
                                <div class="col-3">
                                    <button type="submit" class="btn btn-info" name="eliminarArea">ELIMINAR</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div Class="wrapper" id="area" class="container-fluid" style="display:none;" class="answer_list">
                        <form action="funciones.php" method="POST" enctype="multipart/form-data">
                            <div class="form-group row">
                                <label for="Area" class="col-4 col-form-label">AREA</label>
                                <div class="col-8">
                                    <input type="address" class="form-control" id="area" name="area" placeholder="Area">
                                </div>
                            </div>   
                                                      
                            <div class="form-group row">
                                <label for="formGroupExampleInput" class="col-4 col-form-label">Descripción</label>
                                <div class="col-8">
                                    <textarea class="form-control" rows="3" name="descripcion" id="comment"></textarea>
                                </div>
                            </div>              

                            <div class="form-group row">
                                <div class="col-3">
                                    <button type="submit" name="botonArea" class="btn btn-info">REGISTRAR</button>
                                </div>
                            </div>
                        </form>        
                    </div> 

                    <div Class="wrapper" id="subarea" class="container-fluid" style="display:none;" class="answer_list">
                        <form action="funciones.php" method="POST" enctype="multipart/form-data">
                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-3 col-form-label">AREA</label>
                                <div class="col-9">
                                    <select class="js-example-basic-single" style="width: 100%" name="area">
                                        <?php while ($row = pg_fetch_array($subarealist)):;?>                                                         
                                            <option value=<?php echo $row['id_area'];?>><?php echo $row['nom_area'];?></option>                                                         
                                        <?php endwhile; ?>      
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="Sub Area" class="col-4 col-form-label">SUB AREA</label>
                                <div class="col-8">
                                    <input type="address" class="form-control" id="subarea" name="subAreaReg" placeholder="Sub Area">
                                </div>
                            </div>   
                                                
                            <div class="form-group row">
                                <label for="formGroupExampleInput" class="col-4 col-form-label">Descripción</label>
                                <div class="col-8">
                                    <textarea class="form-control" rows="3" name="subAreaDescripcionReg" id="comment"></textarea>
                                </div>
                            </div>          

                            <div class="form-group row">
                                <div class="col-3">
                                    <button type="submit" name="botonSubArea" class="btn btn-info">REGISTRAR</button>
                                </div>
                            </div>  
                        </form>    
                    </div>     

                    <div class="wrappertable" id="welcomeDiv" class="container-fluid" style="display:none;" class="answer_list">
                    <form action="funciones.php" method="POST" enctype="multipart/form-data"> 
                        <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>>                             
                        <div class="form-group row">
                            <label  class="col-2 col-form-label">BUSCAR</label>
                            <div class="col-sm-10">
                                <select class="js-example-basic-single" style="width: 100%" name="estudiante" value=<?php echo $records1[2]?>>
                                    <?php while ($row = pg_fetch_array($res334)):;?>                                                         
                                        <option>
                                            <?php 
                                                echo $row[1]."&nbsp;&nbsp;&nbsp;" ; echo $row[2]."&nbsp;&nbsp;&nbsp;"; echo $row[3]."&nbsp;&nbsp;&nbsp;";
                                                echo $row[4]."&nbsp;&nbsp;&nbsp;";  echo $row[5]."&nbsp;&nbsp;&nbsp;";
                                            ?>
                                        </option>                                                         
                                    <?php endwhile; ?>														
                                </select>
                            </div>
                        </div>
                        </form>                      
                        <h2 class="tituloespacio">PERFILES NUEVOS REGISTRADOS</h2>                         
                            <div class="row">                               
                            <div class="col-12"></div>                                     
                                <div class="col-12">                               
                                    <div class="scrollable">                              
                                        <form>                                        
                                            <table>
                                            <thead class="thead-dark">
                                            <?php while ($perfil = pg_fetch_array($res333)):;?> 
                                            <tr class="titulotabla">
                                                        <th colspan="2" >Titulo</th>
                                                        <td colspan="10"><?php echo $perfil['titulo'];?></td>
                                                 </tr>
                                                    <tr>
                                                        <th  class="text-center">ID  </th>                                                        
                                                        <th class="text-center">CodSIS  </th>
                                                        <th class="text-center"> Nombre  </th>
                                                        <th class="text-center">Apellido P. </th>
                                                        <th class="text-center">Apellido M. </th> 
                                                        <th class="text-center">Titulo Perfil </th>
                                                        <th class="text-center">Estado </th>
                                                        
                                                </thead>                                                
                                                <tbody>
                                                    <tr>                                                                                                                              
                                                        <td class="text-center"> <?php echo $perfil['id'];   ?></td>  
                                                        <td class="text-center"><?php echo $perfil['cod_sis']; ?></td> 
                                                        <td class="text-center"><?php echo $perfil['nombre']; ?></td>    
                                                        <td class="text-center"><?php echo $perfil['apellidop']; ?></td>     
                                                        <td class="text-center"><?php echo $perfil['apellidom'];?></td>                                                          
                                                        <td class="text-center"><?php echo $perfil['ciclo'];?></td>                                            
                                                        <?php endwhile;?>                                                                                                                                                  
                                                    </tr>
                                                </tbody>                                                    
                                            </table>                                                                                                                             
                                    </div>   
                                    <div class="form-group row">
                                        <div class="col-9"></div>
                                            <div class="col-3">                                                     
                                                <button type="submit" class="btn btn-info "  value="########" name="######">IMPRIMIR</button>                                                                                                   
                                            </div>
                                         </div>            
                                    </div>                                 
                                </div> 
                            </div>                          
                        </form> 
                        <div class="wrappertable" id="listaAreaA" class="container-fluid" style="display:none;" class="answer_list">                    
                        <form action="funciones.php" method="POST" enctype="multipart/form-data"> 
                        <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>> 
                        <input type="hidden" class="list-group-item" name="rol" id="rol" value=<?php echo $_SESSION['rol']?>>
                        <h2 class="tituloespacio">LISTA AREA</h2>                         
                            <div class="row">                               
                            <div class="col-12"></div>                                     
                                <div class="col-12">                               
                                    <div class="scrollable">                                                                
                                            <table>
                                                <thead class="thead-dark">
                                                    <tr>
                                                        <th class="text-center">ID</th>       
                                                        <th class="text-center">Nombre Tabla</th>   
                                                        <th class="text-center"></th>                                                                                                      
                                                    </tr>
                                                </thead>
                                                <?php while ( $tabla =pg_fetch_array($res1191)):;?>  
                                                <tbody>
                                                    <tr>    
                                                        <td class="text-center"> <?php  echo $tabla['id'];?></td>                                                                                                                             
                                                        <td class="text-center"> <?php  echo $tabla['table_name'];?></td>  
                                                        <td class="text-center"> </td>                                                                                                                                                                                                                              
                                                        <td class="text-center">                                                   
                                                            <button type="submit" class="btn btn-outline-danger btn-sm " value="<?php  echo $tabla['table_name'];?>" name="excelSupremo">EXPORTAR</button>                                                                                                   
                                                         </td>                                                                                                                                  
                                                         </tr><?php endwhile;?>     
                                                </tbody>                                                                            
                                            </table>                                                                                                                               
                                    </div>                                   
                                </div>                                
                                </div> 
                                </div>                                                         
                        </form>  

                         <div class="wrappertable" id="listaAreaB" class="container-fluid" style="display:none;" class="answer_list">                    
                        <form action="funciones.php" method="POST" enctype="multipart/form-data"> 
                        <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>> 
                        <input type="hidden" class="list-group-item" name="rol" id="rol" value=<?php echo $_SESSION['rol']?>>
                        <h2 class="tituloespacio">LISTA SUB-AREA</h2>                         
                            <div class="row">                               
                            <div class="col-12"></div>                                     
                                <div class="col-12">                               
                                    <div class="scrollable">                                                                
                                            <table>
                                                <thead class="thead-dark">
                                                    <tr>
                                                        <th class="text-center">ID</th>       
                                                        <th class="text-center">Nombre Tabla</th>   
                                                        <th class="text-center"></th>                                                                                                      
                                                    </tr>
                                                </thead>
                                                <?php while ( $tabla =pg_fetch_array($res1191)):;?>  
                                                <tbody>
                                                    <tr>    
                                                        <td class="text-center"> <?php  echo $tabla['id'];?></td>                                                                                                                             
                                                        <td class="text-center"> <?php  echo $tabla['table_name'];?></td>  
                                                        <td class="text-center"> </td>                                                                                                                                                                                                                              
                                                        <td class="text-center">                                                   
                                                            <button type="submit" class="btn btn-outline-danger btn-sm " value="<?php  echo $tabla['table_name'];?>" name="excelSupremo">EXPORTAR</button>                                                                                                   
                                                         </td>                                                                                                                                  
                                                         </tr><?php endwhile;?>     
                                                </tbody>                                                                            
                                            </table>                                                                                                                               
                                    </div>                                   
                                </div>                                
                                </div> 
                                </div>                                                         
                        </form>  
                        <div class="wrappertable" id="estudiantesSistema" class="container-fluid" style="display:none;" class="answer_list">
                    <form action="funciones.php" method="POST" enctype="multipart/form-data"> 
                        <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>>                             
                        <h2 class="tituloespacio">ESTUDIANTES REGISTRADOS EN EL SISTEMA</h2>  
                        <div class="form-group row">
                            <label  class="col-2 col-form-label">BUSCAR</label>
                            <div class="col-sm-10">
                                <select class="js-example-basic-single" style="width: 100%" name="estudiante" value=<?php echo $records1[2]?>>
                                    <?php while ($row = pg_fetch_array( $recordsDS)):;?>                                                         
                                        <option>
                                            <?php 
                                                echo $row[1]."&nbsp;&nbsp;&nbsp;" ; echo $row[2]."&nbsp;&nbsp;&nbsp;"; echo $row[3]."&nbsp;&nbsp;&nbsp;";
                                                echo $row[4]."&nbsp;&nbsp;&nbsp;";  echo $row[5]."&nbsp;&nbsp;&nbsp;";
                                            ?>
                                        </option>                                                         
                                    <?php endwhile; ?>														
                                </select>
                            </div>
                        </div>
                        </form>                                      
                            <div class="row">                               
                            <div class="col-12"></div>                                     
                                <div class="col-12">                               
                                    <div class="scrollable">                              
                                        <form>                                        
                                            <table>
                                            <thead class="thead-dark">                                                                                 
                                                    <tr>
                                                        <th  class="text-center">ID  </th>                                                        
                                                        <th class="text-center">CodSIS  </th>
                                                        <th class="text-center"> Nombre  </th>
                                                        <th class="text-center">Apellido P. </th>
                                                        <th class="text-center">Apellido M. </th>   
                                                        <th class="text-center">Fecha Nacimiento </th>                                           
                                                </thead>                                                
                                                <tbody>
                                                <?php while ($perfil = pg_fetch_array( $records1DS)):;?>     
                                                    <tr>                                                                                                                              
                                                        <td class="text-center"> <?php echo $perfil['id'];   ?></td>  
                                                        <td class="text-center"><?php echo $perfil['cod_sis']; ?></td> 
                                                        <td class="text-center"><?php echo $perfil['nombre']; ?></td>    
                                                        <td class="text-center"><?php echo $perfil['apellidop']; ?></td>     
                                                        <td class="text-center"><?php echo $perfil['apellidom'];?></td>
                                                        <td class="text-center"><?php echo $perfil['fechanaci'];?></td>                                                          
                                                                                                
                                                        <?php endwhile;?>                                                                                                                                                  
                                                    </tr>
                                                </tbody>                                                    
                                            </table>                                                                                                                             
                                    </div>   
                                    <div class="form-group row">
                                        <div class="col-9"></div>
                                            <div class="col-3">                                                     
                                                <button type="submit" class="btn btn-info "  value="########" name="######">IMPRIMIR</button>                                                                                                   
                                            </div>
                                         </div>            
                                    </div>                                 
                                </div> 
                            </div>                          
                        </form> 
                         

                        <div class="wrappertable" id="docentesSistema" class="container-fluid" style="display:none;" class="answer_list">
                        <form action="funciones.php" method="POST" enctype="multipart/form-data"> 
                        <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>>                             
                        <h2 class="tituloespacio">DOCENTES REGISTRADOS EN EL SISTEMA</h2>  
                        <div class="form-group row">
                            <label  class="col-2 col-form-label">BUSCAR</label>
                            <div class="col-sm-10">
                                <select class="js-example-basic-single" style="width: 100%" name="estudiante" value=<?php echo $records1[2]?>>
                                    <?php while ($row = pg_fetch_array( $recorD)):;?>                                                         
                                        <option>
                                            <?php 
                                                echo $row[1]."&nbsp;&nbsp;&nbsp;" ; echo $row[2]."&nbsp;&nbsp;&nbsp;"; echo $row[3]."&nbsp;&nbsp;&nbsp;";
                                                echo $row[4]."&nbsp;&nbsp;&nbsp;";  echo $row[5]."&nbsp;&nbsp;&nbsp;";
                                            ?>
                                        </option>                                                         
                                    <?php endwhile; ?>														
                                </select>
                            </div>
                        </div>
                        </form>                                      
                            <div class="row">                               
                            <div class="col-12"></div>                                     
                                <div class="col-12">                               
                                    <div class="scrollable">                              
                                        <form>                                        
                                            <table>
                                            <thead class="thead-dark">                                                                                 
                                                    <tr>
                                                        <th  class="text-center">ID  </th>                                                        
                                                        <th class="text-center">CodSIS  </th>
                                                        <th class="text-center"> Nombre  </th>
                                                        <th class="text-center">Apellido P. </th>
                                                        <th class="text-center">Apellido M. </th>   
                                                        <th class="text-center">Fecha Nacimiento </th>                                           
                                                </thead>                                                
                                                <tbody>
                                                <?php while ($perfil1 = pg_fetch_array( $recorD1)):;?>     
                                                    <tr>                                                                                                                              
                                                        <td class="text-center"> <?php echo $perfil1['id'];   ?></td>  
                                                        <td class="text-center"><?php echo $perfil1['cod_sis']; ?></td> 
                                                        <td class="text-center"><?php echo $perfil1['nombre']; ?></td>    
                                                        <td class="text-center"><?php echo $perfil1['apellidop']; ?></td>     
                                                        <td class="text-center"><?php echo $perfil1['apellidom'];?></td>
                                                        <td class="text-center"><?php echo $perfil1['fechanaci'];?></td>                                                          
                                                                                                
                                                        <?php endwhile;?>                                                                                                                                                  
                                                    </tr>
                                                </tbody>                                                    
                                            </table>                                                                                                                             
                                    </div>   
                                    <div class="form-group row">
                                        <div class="col-9"></div>
                                            <div class="col-3">                                                     
                                                <button type="submit" class="btn btn-info "  value="########" name="######">IMPRIMIR</button>                                                                                                   
                                            </div>
                                         </div>            
                                    </div>                                 
                                </div> 
                            </div>                          
                        </form> 
                        


                    <div class="wrapper" id="cambioTema" class="container-fluid" style="display:none;" class="answer_list"> </div>                    
                        
                    <div class="wrapper" id="solicitud" class="container-fluid" style="display:none;" class="answer_list"> </div>
                                                    
                    <div Class="wrapper" id="reg" class="container-fluid" style="display:none;" class="answer_list">
                                            
                    <div class="wrappertable"  id="verificarDocente" class="container-fluid" style="display:none;" class="list"></div>
                                        
                    <div Class="wrapper" id="verificarUsuario" class="container-fluid" style="display:none;" class="answer_list"></div>
                                           
                    <div class="wrapper" id="enviarreg" class="container-fluid" style="display:none;" class="answer_list"></div>			
                                        
                    <div class="wrapper" id="registroperfilDivEstu" class="container-fluid" style="display:none;" class="answer_list"></div> 
                    
                    <div class="wrappertable"  id="cambioTemaAdmin" class="container-fluid" style="display:none;" class="list"></div>

                    <div class="wrapper" id="solicitud" class="container-fluid" style="display:none;" class="answer_list"> </div>

                     <div class="wrapper" id="abandono" class="container-fluid" style="display:none;" class="answer_list"></div>

                     <div class="wrappertable"  id="listaEstudiantesT" class="container-fluid" style="display:none;" class="list"></div>

                     <div class="wrapper" id="cambioTutor" class="container-fluid" style="display:none;" class="answer_list"></div>

                      <div class="wrappertable"  id="cambiotemaEstu" class="container-fluid" style="display:none;" class="list"></div>
                      
                      <div class="wrappertable" id="adminSistema" class="container-fluid" style="display:none;" class="answer_list"></div>

                      <div class="wrappertable" id="exportar" class="container-fluid" style="display:none;" class="answer_list"></div> 

                        <div class="wrappertable" id="bitacoraB" class="container-fluid" style="display:none;" class="answer_list"></div> 

                    <div class="wrappertable" id="bitacoraA" class="container-fluid" style="display:none;" class="answer_list"></div>   

                     <div class="wrapper" id="CambioTutorEstu" class="container-fluid" style="display:none;" class="answer_list"></div>
                 
                </div>				
            </div>
            <div class="footer esconder">
                <p>Copyright © 2018 EKEKO S.A. All rights reserved.</p>
           </div>
        </div>
        <script>
     $( function() {
    $( "#datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true
    });
  } );
     </script> 
    </body>
</html>

