<?php
    session_start();
    if(!isset($_SESSION["usuario"])||$_SESSION["rol"]!=1007)
    {
        header("location:index.php");
    }
    include "funciones.php";    
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/bootstrap-reboot.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"> 
        <link rel="stylesheet" href="css/zerogrid.css" type="text/css" media="screen">
        <link rel="stylesheet" href="css/responsive.css" type="text/css" media="screen">
        <script src="js/jquery.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script> 
   <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>-->
        <link href="css/select2.min.css" rel="stylesheet" />
        <script src="js/select2.min.js"></script> 
        <script src="js/menu.js" type="text/javascript"></script>
        <title>ESTUDIANTE</title>
    </head>

    <body>
        

        <div class="extra">
            <header class="esconder1">
                <div class="container-fluid">
                    <nav class="navbar">
                        <a class="navbar-brand"  target="_blank" rel="noopener noreferrer" href="http://www.umss.edu.bo/">
                            <?php echo '<img height="320" width="320" src="data:image/png;base64,'.$data1[0].' "> ';?>
                        </a>
                        <a class="esconder" target="_blank" rel="noopener noreferrer" href="http://fcyt.umss.edu.bo/">
                            <?php echo '<img height="100" width="100" src="data:image/png;base64,'.$data2[0].' "> ';?>
                        </a>
                    </nav>
                </div>
            </header>

            <header>
                <div class="row">
                    <div class="col-lg-3">
                        <p class="h3 text-muted">
                            <?php echo Ver_DatosIU($_SESSION["usuario"], $_SESSION["rol"]);?>
                        </p> 
                    </div>
                    <div class="col-lg-3"></div>
                    <div class="col-lg-3"></div>								
                    <div class="col-lg-3"> 
                        <a class="navbar-brand" href="logout.php"><input type="submit" name="enviar" value="CERRAR SESION" class="btn btn-success btn-lg"></a>
                    </div>
                </div>                    
            </header> 
                            
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-2 btn-group-vertical">
                        <ul>
                            <button type="button"  name="answer"  onclick="showDivR()" class="btn btn-dark btn-lg espacio1" id="mostrar" value="<?php echo $array114[0]; ?>">COMPLETAR REGISTRO</button>
                            <button type="button" name="answer" onclick="showDiv1()" class="btn btn-dark btn-lg espacio1" id="mostrarP" value="<?php echo $array115[0]; ?>">ASIGNAR TUTOR</button>
                            <button type="button"  name="answer" onclick="showDiv4()"  class="btn btn-dark btn-lg espacio1"  id="mostrarR" value="<?php  ?>">DESARROLLO PERFIL</button>	                                           
                            <button type="button"  name="answer" onclick="showDiv()" class="btn btn-dark btn-lg espacio1"  id="mostrarZ" >REGISTRO PERFIL</button>    
                            <button type="button"  name="answer" onclick="CambioTutorEstu()" class="btn btn-dark btn-lg espacio1"  id="mostrarZ" >CAMBIO TUTOR</button>                         
                            <div class="btn-group espacio1">
                                <button type="button" class="btn btn-dark btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    OPCIONES
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <button class="dropdown-item" type="button" name="answer" onclick="showDivD()">EDITAR MIS DATOS</button>
                                    <form action="funciones.php" method="POST" enctype="multipart/form-data"> 	  
                                         <input type="hidden" class="list-group-item" name="rol" id="rol" value=<?php echo $_SESSION['rol']?>>                                 
                                         <button class="dropdown-item" type="button" name="answer" onclick="myFunction()"  class="btn btn-dark btn-lg espacio1">REGLAMENTO</button>    
                                    </form>
                                    <button class="dropdown-item" type="button" name="answer"  onclick="cambioTema()">CAMBIO TEMA</button>	
                                    <button class="dropdown-item" type="button" name="answer"  onclick="cambioTutor()">CAMBIO TUTOR</button>
                                    <button class="dropdown-item"  type="button" name="answer" data-toggle="modal" data-target=".bd-example-modal-lg" >VERIFICAR</button>                                      
                                </div>
                            </div>
                        </ul>
                    </div>

                    <div class="wrapper" id="registroperfilDiv" class="container-fluid" style="display:none;" class="answer_list">
                         <form action="funciones.php" method="POST" enctype="multipart/form-data">
                          <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>> 
                          <h2 class="centro">REGISTRO PERFIL</h2> 
                          <div class="form-group row">
                                 <div class="col-10"></div>
                                     <div class="col-2">
                                     <button class="btn btn-outline-info btn-sm" type="button" name="answer" data-toggle="modal" data-target=".bd-example-modal-lg" >VERIFICAR</button>   
                                 </div>
                            </div>                                 
                                  <div class="form-group row">
                                      <label for="formGroupExampleInput" class="col-3 col-form-label">TITULO</label>
                                      <div class="col-9">
                                          <input name="nombrePerfil" type="text" class="form-control" id="formGroupExampleInput"  value="<?php echo $tituloPerfil[0];?>">
                                      </div>
                                  </div>
                                  <div class="form-group row">
                                      <label  class="col-3 col-form-label">MODALIDAD</label>
                                      <div class="col-9">
                                          <select class="form-control" name="modalidad" >
                                          <?php                                                          
                                                    while ($row = pg_fetch_array($modalidad)):;?>                                                         
                                                    <option><?php echo $row[0];?></option>                                                         
                                            <?php endwhile;
                                             ?>   
                                          </select>
                                      </div>
                                  </div>
                                  <div class="form-group row">
                                   <label for="exampleFormControlSelect1" class="col-3 col-form-label">AREA</label>
                                      <div class="col-9">
                                           <select class="js-example-basic-single" style="width: 100%" name="area">
                                              <?php                                                          
                                                 while ($row = pg_fetch_array($area)):;?>                                                         
                                                <option><?php echo $row[1];?></option>                                                         
                                              <?php endwhile;
                                               ?>      
                                             </select>
                                          </div>
                                       </div>
                                  <div class="form-group row">
                                      <label for="exampleFormControlSelect1" class="col-3 col-form-label">SUB-AREA</label>
                                      <div class="col-9">
                                      <select class="js-example-basic-single" style="width: 100%" name="subarea">
                                          <?php                                                          
                                                 while ($row = pg_fetch_array($subarea)):;?>                                                         
                                                <option><?php echo $row[1];?></option>                                                         
                                              <?php endwhile;
                                               ?>      
                                          </select>
                                      </div>
                                  </div>
                                  <div class="form-group row">
                                      <label for="formGroupExampleInput" class="col-3 col-form-label">OBJETIVO GENERAL</label>
                                      <div class="col-9">
                                          <textarea name="general" class="form-control" rows="2" id="objetivoC"></textarea>
                                      </div>
                                  </div><div class="form-group row">
                                      <label for="formGroupExampleInput" class="col-3 col-form-label">OBJETIVOS ESPECIFICOS</label>
                                      <div class="col-9">
                                          <textarea name="especifico" class="form-control" rows="2" id="comment"></textarea>
                                      </div>
                                  </div>
                                  <div class="form-group row">
                                      <label for="formGroupExampleInput" class="col-3 col-form-label">DESCRIPCION</label>
                                      <div class="col-9">
                                          <textarea name="descripcion" class="form-control" rows="2" id="comment"></textarea>
                                      </div>
                                  </div>
                                  <div class="form-group row">
                                      <div class="col-3">
                                          <button type="submit" name="registroPerfil" class="btn btn-danger">ENVIAR</button>
                                                                               
                                      </div>
                                  </div>
                         </form>
                     </div>                       

                    <div class="wrapper" id="welcomeDiv" class="container-fluid" style="display:none;" class="answer_list">
                        <form action="funciones.php" method="POST" enctype="multipart/form-data">
                            <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>>                   
                            <div class="form-group row">                            
                                <label for="exampleFormControlSelect1" class="col-2 col-form-label">MI TUTOR</label>
                                <div class="col-sm-10">
                                    <select class="js-example-basic-single" style="width: 100%" name="tutor">
                                        <?php while ($row = pg_fetch_array($tutor)):;?>                                                         
                                            <option value=<?php echo $row[0];?>><?php echo $row[1]; echo $row[2]; echo $row[3];?></option>                                                                                                         
                                        <?php endwhile; ?>														
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-2 col-form-label">TITULO PERFIL</label>
                                <div class="col-10">
                                    <input type="text" class="form-control" id="titulo" name="titulo">
                                </div>
                            </div>
                                            
                            <div class="form-group row">
                                <label  class="col-2 col-form-label">PDF</label>
                                <div class="col-9">                                         
                                      <input type="file" name="fileToUpload" id="fileToUpload">                                                                                            
                                </div>
                                <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#AyudaEnviarRevision">?</button>  
                            </div>                                                       
                                                
                            <div class="form-group row ">
                                <label for="formGroupExampleInput" class="col-2 col-form-label">NOTA</label>
                                <div class="col-10">
                                    <textarea class="form-control" rows="5" id="comment" name="comentario"></textarea>
                                </div>
                            </div>                               
                                                
                            <div class="form-group row">
                                <div class="col-sm-10">
                                    <button type="submit" name="submitSolicitud" class="btn btn-info">ENVIAR SOLICITUD</button>
                                </div>
                            </div>
                        </form>
                    </div>

                         <div class="wrapper" id="CambioTutorEstu" class="container-fluid" style="display:none;" class="answer_list">
                         <h2 class="centro">Esta ventana se habilitara en casos extremos donde su tutor haya solicitado o su estado como tutor no seran posibles.<br>
                         Usted debe asignar a un nuevo tutor de la siguiente lista.</h2>  
                        <form action="funciones.php" method="POST" enctype="multipart/form-data">
                            <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>>                   
                            <div class="form-group row">                            
                                <label for="exampleFormControlSelect1" class="col-2 col-form-label">NUEVO TUTOR</label>
                                <div class="col-sm-10">
                                    <select class="js-example-basic-single" style="width: 100%" name="tutor">
                                        <?php while ($row = pg_fetch_array($tutor1272018)):;?>                                                         
                                            <option value=<?php echo $row[0];?>><?php echo $row[1]; echo $row[2]; echo $row[3];?></option>                                                                                                         
                                        <?php endwhile; ?>														
                                    </select>
                                </div>
                            </div>                                       
                            <div class="form-group row">
                                <div class="col-sm-10">
                                    <button type="submit" name="#################################" class="btn btn-info">ENVIAR SOLICITUD</button>
                                </div>
                            </div>
                        </form>
                    </div>


                <div Class="wrapper" id="editardatos" class="container-fluid" style="display:none;" class="answer_list">
                        <form action="funciones.php" method="POST">
                            <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>> 
                            <input type="hidden" class="list-group-item" name="rol" id="rol" value=<?php echo $_SESSION['rol']?>> 
                            <h2 class="centro">ACTUALIZAR O EDITAR MIS DATOS</h2>  
                            <div class="form-group row">
                                <label for="formGroupExampleInput" class="col-4 col-form-label">NOMBRE</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="nombre" name="nombre"  value="<?php echo $linea[3];?>">
                                </div>
                            </div>
            
                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-4 col-form-label">APELLIDO PATERNO</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="apellidop" name="apellidop"  value="<?php  echo $linea[4];?>">		
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-4 col-form-label">APELLIDO MATERNO</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="apellidom" name="apellidom"  value="<?php  echo $linea[5];?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-4 col-form-label">FECHA NACIMIENTO</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="fec_nac" name="fec_nac" value="<?php echo $linea[6];?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="formGroupExampleInput" class="col-4 col-form-label">TELEFONO</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="telefono" name="telefono" value="<?php echo $linea[7];?>">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="formGroupExampleInput" class="col-4 col-form-label">EMAIL</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="correo" name="correo" value="<?php echo $linea[8];?>">
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <div class="col-3">
                                    <button type="submit" class="btn btn-info" name="env_est">ENVIAR</button>
                                </div>
                            </div>
                        </form>
                    </div>
                                    
                    <div class="wrapper" id="enviarreg" class="container-fluid" style="display:none;" class="answer_list">
                        <form action="funciones.php" method="POST" enctype="multipart/form-data">
                            <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>>                           
                            <div class="form-group row">
                                <label class="col-3 col-form-label">MI TUTOR</label>
                                <div class="col-9">
                                    <label><?php echo $array116[0]; echo $array116[1]; echo $array116[2];?></label>       
                                </div>
                            </div>                           
                             <div class="form-group row">
                                <label class="col-3 col-form-label">TITULO PERFIL</label>
                                <div class="col-9">
                                    <input type="text" class="form-control" id="titulo" name="" value="<?php echo $tituloPerfil[0];?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3 col-form-label">REVISION</label>
                                <div class="col-9">
                                    <input type="text" name="tituloPDF" class="form-control" id="titulo" placeholder="Revision1 codigoSIS">                                    
                                </div>                               
                            </div>
                                    
                            <div class="form-group row">
                                <label  class="col-3 col-form-label">PDF</label>
                                <div class="col-8">                                         
                                     <input type="file" name="fileToUpload" id="fileToUpload">                                                    
                                </div>
                                <button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#AyudaEnviarRevision">?</button>
                            </div>                                                          
    
                            <div class="form-group row ">
                                <label for="formGroupExampleInput" class="col-3 col-form-label">COMENTARIOS</label>
                                <div class="col-9">
                                    <textarea class="form-control" name="comentPDF" rows="5" id="comment"></textarea>
                                </div>
                            </div>
        
                            <div class="form-group row">
                                <div class="col-3">
                                    <button type="submit" name="subirPDF" value="Upload" class="btn btn-info">ENVIAR</button>
                                </div>
                            </div>
                        </form>
                    </div> 
                                    
                    <div class="wrapper" id="cambioTema" class="container-fluid" style="display:none;" class="answer_list">
                        <form action="funciones.php" method="POST" enctype="multipart/form-data">
                            <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>>                            
                            <h2 class="temporal" col>EL FORMULARIO ESTA EN CONSTRUCCION CON 80% DE AVANCE, SOLO FALTA LA DEBIDA ADAPTACION DE LA BD Y UNA NUEVA TABLA. DISCULPAS</h2>  
                            <div class="form-group row">
                                <label class="col-3 col-form-label">CAMBIO</label>
                                <div class="col-8">
                                     <div class="form-check form-check-inline">
                                        <label class="container1">
                                                LEVE
                                        <input type="radio" name="radio" checked="checked" value="1">
                                        <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline radioboton">
                                        <label class="container1">
                                            TOTAL
                                        <input type="radio" name="radio" value="2">
                                        <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div><button class="btn btn-outline-primary float-right " onclick="########" >?</button>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">TITULO</label>
                                <div class="col-9">
                                    <input type="text" name="titulo" class="form-control" id="titulo"  value="<?php echo $tituloPerfil[0];?>">
                                </div>
                            </div>
        
                            <div class="form-group row">
                                <label  class="col-3 col-form-label">ELEGIR</label>
                                <div class="col-9">                                         
                                    <input type="file" name="pdf" size="50" />
                                    <button class="btn btn-outline-primary float-right " onclick="########" >?</button>                                                         
                                </div>
                            </div>                                                          
    
                            <div class="form-group row ">
                                <label for="formGroupExampleInput" class="col-3 col-form-label">COMENTARIOS</label>
                                <div class="col-9">
                                    <textarea class="form-control" name="comentario" rows="5" id="comment"></textarea>
                                </div>
                            </div>            
                            <div class="form-group row">
                                <div class="col-3">
                                    <button type="submit" name="cambio" value="Upload" class="btn btn-info">ENVIAR</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    
                    <div class="wrapper" id="cambioTutor" class="container-fluid" style="display:none;" class="answer_list">
                        <form action="" method="POST" enctype="multipart/form-data">
                            <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>>  
                            <h2 class="temporal" col>EL FORMULARIO ESTA EN CONSTRUCCION CON 80% DE AVANCE, SOLO FALTA LA DEBIDA ADAPTACION DE LA BD Y UNA NUEVA TABLA. DISCULPAS</h2>  
                            <h2 class="centro">CAMBIO DE TUTOR</h2> 
                            <div class="form-group row">
                                <label class="col-3 col-form-label">MI TUTOR 1</label>
                                <div class="col-9">
                                    <input type="text" class="form-control" id="titulo" name="###############">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">MI TUTOR 2</label>
                                <div class="col-9">
                                    <input type="text" class="form-control" id="titulo" name="############">
                                </div>
                            </div>
        
                            <div class="form-group row">
                                <label  class="col-3 col-form-label">ELEGIR</label>
                                <div class="col-9">                                         
                                    <input type="file" name="pdf" size="50" />
                                    <button class="btn btn-outline-danger btn-sm float-right " onclick="########" >?</button>                                                         
                                </div>
                            </div>                                                          
    
                            <div class="form-group row ">
                                <label for="formGroupExampleInput" class="col-3 col-form-label">COMENTARIOS</label>
                                <div class="col-9">
                                    <textarea class="form-control" name="##########" rows="5" id="comment">
                                    </textarea>
                                </div>
                            </div>            
                            <div class="form-group row">
                                <div class="col-3">
                                    <button type="submit" name="##########" value="Upload" class="btn btn-info">ENVIAR</button>
                                </div>
                            </div>
                        </form>
                    </div> 

                    <div class="wrapper" id="registroperfilDivEstu" class="container-fluid" style="display:none;" class="answer_list">
                        <form action="funciones.php" method="POST" enctype="multipart/form-data"> 
                        <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>>                             
                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-sm-2 col-form-label">CARRERA</label>
                                <div class="col-sm-10">
                                    <select class="form-control" id="exampleFormControlSelect1" name="carrera">
                                        <?php while ($row = pg_fetch_array($carrera)):;?>                                                         
                                            <option value=<?php echo $row[0];?>><?php echo $row[1];?></option>                                                         
                                        <?php endwhile; ?>      
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-sm-2 col-form-label">EMAIL</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="exampleInputEmail1" name="email" aria-describedby="emailHelp" placeholder="Ingresar Email">                        
                                        </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-sm-2 col-form-label">CELULAR</label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" id="example-tel-input" name="telefono"  placeholder="Ingresar celular">                               
                                </div>
                            </div>
                                    
                            <div class="form-group row">
                                <div class="col-sm-10">
                                    <button type="submit" name="completarRegisEstu" class="btn btn-info">ENVIAR</button>
                                </div>
                            </div>
                        </form>
                    </div> 
                  
                

                      <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg">                                                     
                                                     <div class="container-fluid">   
                                                           <div class="modal-content">
                                                           <div class="form-group row">
                                                           <h3 style="color: Tomato;">Los campos seran visibles a medida avance en el envio de todos sus registros.</h3>                                                                                
                                                            </div>
                                                               <h5><span class="label label-default">UNIVERSIDAD MAYOR DE SAN SIMON</span></h5> 
                                                               <h5><span class="label label-default">FACULTAD DE CIENCIAS Y TECNOLOGIA</span></h5>
                                                               <h5 class="botonEspacio1"><span class="label label-default">CARRERAS DE ING. SISTEMAS E INFORMATICA</span></h5> 
                                                               <h5 class="text-center botonEspacio1"><span class="label label-default">FORMULARIO APROBACION TEMA DE PROYECTO FINAL</span></h5>                                                                                                                
                                                              
                                                               <style type="text/css">
                                                                    .tg  {border-collapse:collapse;border-spacing:0;}
                                                                    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
                                                                    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
                                                                    .tg .tg-1wig{font-weight:bold;text-align:left;vertical-align:top}
                                                                    .tg .tg-baqh{text-align:center;vertical-align:top}
                                                                    .tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}
                                                                    .tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
                                                                    .tg .tg-0lax{text-align:left;vertical-align:top}
                                                                    .tg .tg-amwm{font-weight:bold;text-align:center;vertical-align:top}
                                                                    .tg .tg-jpc1{font-size:10px;text-align:left;vertical-align:top}
                                                                        </style>
                                                                            <table class="tg">
                                                                                <tr>
                                                                                    <th class="tg-0pky" colspan="2">Nombre estudiante:</th>
                                                                                    <th class="tg-0pky" colspan="6"><?php echo""."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                                                                                        echo $linea[4]."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                                                                                        echo $linea[5]."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                                                                                        echo $linea[3]."&nbsp;&nbsp;&nbsp;" ;?></th>
                                                                                    </tr>
                                                                                    <tr>
                                                                                         <td class="tg-0pky" colspan="2"></td>
                                                                                         <td class="tg-c3ow" colspan="2">Ap. Paterno</td>
                                                                                         <td class="tg-baqh" colspan="2">Ap. Materno</td>
                                                                                         <td class="tg-baqh" colspan="2">Nombres</td>
                                                                                    </tr>
                                                                                     <tr>
                                                                                         <td class="tg-0pky" colspan="3">Telefóno:  <?php echo""."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";echo $array127[0];?></td>
                                                                                         <td class="tg-0pky" colspan="5">Email:  <?php echo""."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"; echo $array127[1];?></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                         <td class="tg-0pky" colspan="8">Tutor: <?php echo""."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"; echo $array111[0]; echo $array111[1];echo $array111[2];?></td>
                                                                                     </tr>
                                                                                     <tr>
                                                                                         <td class="tg-0lax" colspan="4">Carrera: <?php echo""."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"; echo $array112[0];?></td>
                                                                                     <td class="tg-0lax" colspan="4">Trabajo Conjunto:</td>
                                                                                    </tr>
                                                                                     <tr>
                                                                                         <td class="tg-0lax" colspan="4">Gestion de Aprobación:</td>
                                                                                         <td class="tg-0lax" colspan="4">Cambio de tema:<br>Cambio Leve:</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                         <td class="tg-0lax" colspan="8"></td>
                                                                                     </tr>
                                                                                    <tr>
                                                                                         <td class="tg-0lax" colspan="8">Título: <?php echo $array113[0];?></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="tg-0lax" colspan="3">Área: <?php echo $array113[1];?></td>
                                                                                        <td class="tg-0lax" colspan="5">Subárea: <?php echo $array113[2];?></td>
                                                                                    </tr>
                                                                                     <tr>
                                                                                        <td class="tg-0lax" colspan="8">Modalidad: <?php echo $array113[3];?></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                         <td class="tg-0lax" colspan="8">Objetivo general:<?php $newtext = wordwrap($array113[4], 70, "<br>", true); echo "$newtext<br>"; ?><br><br></td>
                                                                                    </tr>
                                                                                     <tr>
                                                                                        <td class="tg-0lax" colspan="8">Objetivos especificos:<?php $newtext = wordwrap($array113[5], 70, "<br>", true); echo "$newtext<br>"; ?><br><br></td>
                                                                                     </tr>
                                                                                    <tr>
                                                                                         <td class="tg-0lax" colspan="8">Descripción:<?php $newtext = wordwrap($array113[6], 70, "<br>", true); echo "$newtext<br>"; ?><br><br></td>
                                                                                    </tr>
                                                                                     <tr>
                                                                                         <td class="tg-0lax" colspan="8"></td>
                                                                                     </tr>
                                                                                      <tr>
                                                                                         <td class="tg-0lax" colspan="2"><br><br></td>
                                                                                         <td class="tg-0lax" colspan="2"><br><br></td>
                                                                                         <td class="tg-0lax" colspan="2"><br><br></td>
                                                                                         <td class="tg-0lax" colspan="2"><br><br></td>
                                                                                     </tr>
                                                                                     <tr>
                                                                                         <td class="tg-amwm" colspan="2">Director de Carrera</td>
                                                                                         <td class="tg-amwm" colspan="2">Docente Materia</td>
                                                                                         <td class="tg-amwm" colspan="2">Tutor</td>
                                                                                         <td class="tg-amwm" colspan="2">Estudiante</td>
                                                                                    </tr>
                                                                                     <tr>
                                                                                         <td class="tg-0lax" colspan="8"></td>
                                                                                    </tr>
                                                                                     <tr>
                                                                                         <td class="tg-jpc1" colspan="4"><span style="font-style:italic">Registrado por: Consultar el nombre del encargado de </span><br><span style="font-style:italic">registro de perfil en el Laboratio de Desarrollo INF-SIS</span></td>
                                                                                         <td class="tg-0lax" colspan="2">Firma:</td>
                                                                                         <td class="tg-0lax" colspan="2">Fecha:</td>
                                                                                    </tr>                                                                                   
                                                                          </table>                                                                         
                                                                           <!--<div class="form-group row">
                                                                               <label for="formGroupExampleInput" class="col-9 col-form-label">PRESIONAR CONFIRMAR SIGNIFICA EL ENVIO DEFINITIVO DE SU PERFIL</label>
                                                                                 <div class="col-3">
                                                                                     <button type="submit" name="registroPerfil" class="btn btn-danger">CONFIRMAR</button>
                                                                                 </div>
                                                                               </div> -->                                                                         
                                                               </div>
                                                        </div>
                                                    </div>                                               
                                               </div>
                                                                                                            <!-- Modal -->
                                            <div class="modal fade" id="CompletarRegistro" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                 <div class="modal-dialog modal-dialog-centered" role="document">
                                                     <div class="modal-content">
                                                       <div class="modal-header">
                                                           <h5 class="modal-title" id="exampleModalLongTitle">AYUDA</h5>
                                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                              </button>
                                                        </div>
                                                    <div class="modal-body">
                                                            Actualizar o editar sus datos.
                                                     </div>
                                                          
                                                      </div>
                                                </div>
                                            </div>  
                                                                                                            <!-- Modal -->
                                            <div class="modal fade" id="AyudaEnviarRevision" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                 <div class="modal-dialog modal-dialog-centered" role="document">
                                                     <div class="modal-content">
                                                       <div class="modal-header">
                                                           <h5 class="modal-title" id="exampleModalLongTitle">AYUDA</h5>
                                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                              </button>
                                                        </div>
                                                    <div class="modal-body">
                                                          EL sistema no aceptara dos PDFs con el mismo nombre, Sugerimos añadir su codigoSIS
                                                          y el nombre estara bajo su criterio.<br>
                                                          Ejemplo primera revison: Revision1-20152120.pdf o Revision-A-20152120.pdf.<br>
                                                          Ejemplo segunda revison: Revision2-20152120.pdf o RevisionB20152120.pdf.
                                                     </div>
                                                          
                                                      </div>
                                                </div>
                                            </div>  
                            
                    <div class="wrapper" id="solicitud" class="container-fluid" style="display:none;" class="answer_list"> </div>
                    
                    <div Class="wrapper" id="eliminarSubArea" class="container-fluid" style="display:none;" class="answer_list"></div>
            
                    <div Class="wrapper" id="eliminarArea" class="container-fluid" style="display:none;" class="answer_list"></div>
                    
                    <div Class="wrapper" id="subarea" class="container-fluid" style="display:none;" class="answer_list"></div>
            
                    <div Class="wrapper" id="verificarUsuario" class="container-fluid" style="display:none;" class="answer_list"></div>
                    
                    <div Class="wrapper" id="area" class="container-fluid" style="display:none;" class="answer_list"></div>
                    
                    <div Class="wrapper" id="reg" class="container-fluid" style="display:none;" class="answer_list"></div>
                    
                    <div class="wrappertable"  id="verificarDocente" class="container-fluid" style="display:none;" class="list"></div>
                    
                    <div Class="wrapper" id="editarArea" class="container-fluid" style="display:none;" class="answer_list"></div>
                    
                    <div Class="wrapper" id="editarSubArea" class="container-fluid" style="display:none;" class="answer_list"></div>

                     <div class="wrappertable"  id="cambioTemaAdmin" class="container-fluid" style="display:none;" class="list"></div>

                    <div class="wrapper" id="solicitud" class="container-fluid" style="display:none;" class="answer_list"> </div>

                     <div class="wrapper" id="abandono" class="container-fluid" style="display:none;" class="answer_list"></div>

                     <div class="wrappertable"  id="listaEstudiantesT" class="container-fluid" style="display:none;" class="list"></div>

                     <div class="wrappertable"  id="cambiotemaEstu" class="container-fluid" style="display:none;" class="list"></div>
                     
                     <div class="wrappertable" id="estudiantesSistema" class="container-fluid" style="display:none;" class="answer_list"></div>

                     <div class="wrappertable" id="docentesSistema" class="container-fluid" style="display:none;" class="answer_list"></div>

                     <div class="wrappertable" id="adminSistema" class="container-fluid" style="display:none;" class="answer_list"></div>

                     <div class="wrappertable" id="exportar" class="container-fluid" style="display:none;" class="answer_list"></div> 

                      <div class="wrappertable" id="bitacoraB" class="container-fluid" style="display:none;" class="answer_list"></div> 

                       <div class="wrappertable" id="bitacoraA" class="container-fluid" style="display:none;" class="answer_list"></div>  

                         <div class="wrappertable" id="listaAreaA" class="container-fluid" style="display:none;" class="answer_list"></div>

                             <div class="wrappertable" id="listaAreaB" class="container-fluid" style="display:none;" class="answer_list"></div>


                                            
                </div>
            </div>
            <div class="footer esconder">
              <p>Copyright © 2018 EKEKO S.A. All rights reserved.</p>
           </div>
        </div>
    </body>

<script>
          myFunction2();
                 
 </script>
</html>

