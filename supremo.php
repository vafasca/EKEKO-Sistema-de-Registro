<?php
session_start();
if(!isset($_SESSION["usuario"])||$_SESSION["rol"]!=1001)
{
header("location:index.php");
}
include "funciones.php";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/bootstrap-reboot.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
        <link rel="stylesheet" href="css/zerogrid.css" type="text/css" media="screen">
        <link rel="stylesheet" href="css/responsive.css" type="text/css" media="screen">
        <script src="js/jquery.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <link href="css/select2.min.css" rel="stylesheet" />
        <script src="js/select2.min.js"></script> 
        <script src="js/menu.js" type="text/javascript"></script>
        <title>SUPREMO</title>
    </head>

    <body>
        <div class="extra">
            <header>
                <div class="container-fluid">
                    <nav class="navbar">
                        <a class="navbar-brand"   target="_blank" rel="noopener noreferrer"href="http://www.umss.edu.bo/">
                        <?php echo '<img height="600 width="600" src="data:image/png;base64,'.$data1[0].' "> ';?>
                        </a>
                        <a class="esconder"  target="_blank" rel="noopener noreferrer" href="http://fcyt.umss.edu.bo/">
                        <?php echo '<img height="120" width="140" src="data:image/png;base64,'.$data2[0].' "> ';?>
                        </a>
                    </nav>
                </div>
            </header>

            <header>
                                <div class="row">
                                <div class="col-lg-3"><p class="h3 text-muted"><?php echo Ver_DatosIU($_SESSION["usuario"], $_SESSION["rol"]);?></p> </div>
                                <div class="col-lg-3"></div>
                                <div class="col-lg-3"></div>								
                                <div class="col-lg-3"> 
                                    <a class="navbar-brand" href="logout.php"><input type="submit" name="enviar" value="CERRAR SESION" class="btn btn-success btn-lg"></a>
                                </div>
                                </div>
                                
            </header>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-2 btn-group-vertical">
                        <ul>
                            <button type="button"  name="answer"  onclick="showDivR()" class="btn btn-dark btn-lg espacio1" id="mostrar">COMPLETAR REGISTRO</button>
                            <button type="button" name="answer" onclick="showDiv()" class="btn btn-dark btn-lg espacio1">REGISTRAR</button>
                            <button type="button" name="answer" onclick="exportar()" class="btn btn-dark btn-lg espacio1">EXPORTAR</button> 
                            <div class="btn-group espacio1">
                            <button type="button" class="btn btn-dark btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">BITACORA</button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <button class="dropdown-item" type="button" name="answer" onclick="bitacoraA()"  class="btn btn-light espacio1">BITACORA A</button>  
                                    <button  class="dropdown-item"  type="button" name="answer" onclick="bitacoraB()" class="btn btn-light">BITACORA B</button>                                    
                                </div>
                                </div>
                            <div class="btn-group espacio1">
                            <button type="button" class="btn btn-dark btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">NOTIFICACIONES</button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <button class="dropdown-item" type="button" name="answer"  onclick="showDivVerificar()" class="btn btn-light "> Habilitar Estudiante</button>
                                    <button class="dropdown-item" type="button" name="answer"  onclick="showDivVerificarDocente()" class="btn btn-light">Habilitar Docente</button>    
                                    <button class="dropdown-item" type="button" name="answer" onclick="showDiv1()" class="btn btn-light espacio1">Perfiles Registrados</button>  
                                    <button  class="dropdown-item"  type="button" name="answer" onclick="estudiantesSistema()" class="btn btn-light">Lista Estudiantes</button>
                                    <button  class="dropdown-item"  type="button" name="answer" onclick="docentesSistema()" class="btn btn-light">Lista Docentes</button>
                                    <button  class="dropdown-item"  type="button" name="answer" onclick="adminSistema()" class="btn btn-light">Administradores</button>
                                </div>
                                </div>
                                <div class="btn-group espacio1">
                            <button type="button" class="btn btn-dark btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">LISTA AREA Y SUB-AREA</button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <button class="dropdown-item" type="button" name="answer" onclick="listaAreaA()" class="btn btn-light espacio1">LISTA AREA</button>  
                                    <button  class="dropdown-item"  type="button" name="answer" onclick="listaAreaB()" class="btn btn-light">LISTA SUB-AREA</button>                                    
                                </div>
                                </div>                   
                            <div class="btn-group espacio1">
                                    <button type="button" class="btn btn-dark btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">AREA Y SUB-AREA</button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <button class="dropdown-item" type="button" name="answer" onclick="showDiv2()" class="btn btn-light">Agregar Area</button>  
                                        <button class="dropdown-item" type="button" name="answer" onclick="showSubArea()" class="btn btn-light">Agregar Sub-area</button>                                 
                                        <button class="dropdown-item" type="button" name="answer" onclick="editarArea()" class="btn btn-light">Editar Area</button> 
                                        <button class="dropdown-item" type="button" name="answer" onclick="editarSubArea()" class="btn btn-light">Editar Sub-area</button>   
                                        <button class="dropdown-item" type="button" name="answer" onclick="showDeleteArea()" class="btn btn-light">Eliminar Area</button> 
                                        <button class="dropdown-item" type="button" name="answer" onclick="showDeleteSubArea()" class="btn btn-light">Eliminar Sub-area</button>                                   
                                    </div>
                                </div>     
                            
                            <button type="button" name="answer" onclick="showDiv3()" class="btn btn-dark btn-lg espacio1">DIRECTOR CARRERA</button> 
                           
                            <div class="btn-group espacio1">
                                <button type="button" class="btn btn-dark btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">OPCIONES</button>
                                <div class="dropdown-menu dropdown-menu-right">
                                     <button class="dropdown-item" type="button" name="answer" onclick="myFunction()"  class="btn btn-dark btn-lg espacio1">REGLAMENTO</button>                                   
                                    <button class="dropdown-item" type="button" onclick="showDivD()" class="btn btn-dark btn-lg espacio1">EDITAR MIS DATOS</button>
                                </div>
                            </div>
                        </ul>
                    </div>
                    
                  
                                       
                    <div Class="wrapper" id="reg" class="container-fluid" style="display:none;" class="answer_list">
                         <form action="funciones.php" method="POST">
                            <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>> 
                            <input type="hidden" class="list-group-item" name="rol" id="rol" value=<?php echo $_SESSION['rol']?>> 
                          
                            <div class="form-group row">                               
                                <div class="col-11">                                         
                                     <h2 class="tituloespacio">ACTUALIZAR DIRECTOR DE CARRERA</h2>                                                                                             
                                </div>
                                <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#AyudaActualizar">?</button>  
                            </div>     
                            <div class="form-group row">
                                <label for="formGroupExampleInput" class="col-4 col-form-label">DIRECTOR DE CARRERA</label>
                                <div class="col-8">
                                     <label for="formGroupExampleInput" > <?php  echo $directorCa[2];?> <?php  echo $directorCa[3];?>  <?php  echo $directorCa[4];?></label>
                                </div>
                            </div>
            
                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-4 col-form-label">NUEVO DIRECTOR DE CARRERA</label>
                                <div class="col-sm-8">
                                <select class="js-example-basic-single" style="width: 100%" name="nuevoDir">
                                    <?php while ($row = pg_fetch_array($recordsDocente3)):;?>                                                         
                                        <option value=<?php echo $row[0];?>>
                                            <?php 
                                                echo $row[3]."&nbsp;&nbsp;&nbsp;";
                                                echo $row[4]."&nbsp;&nbsp;&nbsp;";  echo $row[5]."&nbsp;&nbsp;&nbsp;";
                                            ?>
                                        </option>                                                         
                                    <?php endwhile; ?>														
                                </select>
                            </div>
                        </div>
                            <div class="form-group row">
                                <div class="col-3">
                                    <button type="submit" class="btn btn-danger" name="actualizarDirector">ACTUALIZAR</button>
                                </div>
                            </div>
                        </form>
                        </div>

                        
                      <div class="wrapper" id="registroperfilDivEstu" class="container-fluid" style="display:none;" class="answer_list">
                        <form action="funciones.php" method="POST" enctype="multipart/form-data"> 
                        <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>>                             
                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-sm-2 col-form-label">CARRERA</label>
                                <div class="col-sm-10">
                                    <select class="form-control" id="exampleFormControlSelect1" name="carrera">
                                        <?php while ($row = pg_fetch_array($carrera)):;?>                                                         
                                            <option value=<?php echo $row[0];?>><?php echo $row[1];?></option>                                                         
                                        <?php endwhile; ?>      
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-sm-2 col-form-label">EMAIL</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="exampleInputEmail1" name="email" aria-describedby="emailHelp" placeholder="Ingresar Email">                        
                                        </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-sm-2 col-form-label">CELULAR</label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" id="example-tel-input" name="telefono"  placeholder="Ingresar celular">                               
                                </div>
                            </div>
                                    
                            <div class="form-group row">
                                <div class="col-sm-10">
                                    <button type="submit" name="manhatam1" class="btn btn-info">ENVIAR</button>
                                </div>
                            </div>
                        </form>
                    </div> 

                    <div class="wrapper" id="registroperfilDiv" class="container-fluid" style="display:none;" class="answer_list">
                        <form acion="funciones.php" method="POST">
                            <div class="form-group">
                                <div class="form-check form-check-inline">
                                    <label class="container1">
                                        ESTUDIANTE
                                        <input type="radio" checked="checked" name="radio" value="1007">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                
                                <div class="form-check form-check-inline">
                                    <label class="container1">
                                        DOCENTE
                                        <input type="radio" name="radio" value="1004">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                
                           <!--     <div class="form-check form-check-inline">
                                    <label class="container1">
                                        ADMINISTRATIVO
                                        <input type="radio" name="radio">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>-->
                            </div>
                 
                            <div class="form-group row">
                                <label for="Codigo Sis" class="col-4 col-form-label">Codigo Sis</label>
                                <div class="col-8">
                                    <input type="address" class="form-control" name="codsu" id="sname" placeholder="Codigo Sis">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="Celula de Identidad" class="col-4 col-form-label">Ingresar C.I.</label>
                                <div class="col-8">
                                    <input type="address" class="form-control" name="cisu" id="sname" placeholder="Ingresar C.I.">
                                </div>
                            </div>
                            <div class="form-group row">
                            <label for="Nombres" class="col-4 col-form-label">Contraseña</label>
                                <div class="col-8">
                                <input type="name" class="form-control" id="fname" name="consu" placeholder="Ingresar Contraseña">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Nombres" class="col-4 col-form-label">Nombres</label>
                                <div class="col-8">
                                    <input type="address" class="form-control" name="nomsu" id="sname" placeholder="Nombres">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="Apellido Paterno" class="col-4 col-form-label">Apellido Paterno</label>
                                <div class="col-8">
                                    <input type="address" class="form-control" name="pasu" id="sname" placeholder="Apellido Paterno">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="Apellido Materno" class="col-4 col-form-label">Apellido Materno</label>
                                <div class="col-8">
                                    <input type="address" class="form-control" name="masu" id="sname" placeholder="Apellido Materno">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="Fecha Nacimiento" class="col-4 col-form-label">Fecha Nacimiento</label>
                                <div class="col-8">
                                    <input type="text" id="datepicker" name="fsu" class="form-control" placeholder="Elegir">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-3">
                                    <button type="reg_adm" name="btnresu" class="btn btn-info">Registrar</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="wrappertable" id="welcomeDiv" class="container-fluid" style="display:none;" class="answer_list">
                    <form action="funciones.php" method="POST" enctype="multipart/form-data"> 
                        <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>>   
                        <h2 class="temporal" col>LAS TABLAS DE EXCEL SON VISIBLES DE FORMA NORMAL DE MANERA LOCAL EN XAMPP, CREEMOS QUE SE DEBE A EL FIREWALL, BUG O MOTIVOS DE SEGURIDAD QUE NO SON VISIBLES MEDIANTE LA WEB.</h2>  
                          
                        <div class="form-group row">
                            <label  class="col-2 col-form-label">BUSCAR</label>
                            <div class="col-sm-10">
                                <select class="js-example-basic-single" style="width: 100%" name="estudiante" value=<?php echo $records1[2]?>>
                                    <?php while ($row = pg_fetch_array($res334)):;?>                                                         
                                        <option>
                                            <?php 
                                                echo $row[1]."&nbsp;&nbsp;&nbsp;" ; echo $row[2]."&nbsp;&nbsp;&nbsp;"; echo $row[3]."&nbsp;&nbsp;&nbsp;";
                                                echo $row[4]."&nbsp;&nbsp;&nbsp;";  echo $row[5]."&nbsp;&nbsp;&nbsp;";
                                            ?>
                                        </option>                                                         
                                    <?php endwhile; ?>														
                                </select>
                            </div>
                        </div>
                        </form>                      
                        <h2 class="tituloespacio">PERFILES NUEVOS REGISTRADOS</h2>                         
                            <div class="row">                               
                            <div class="col-12"></div>                                     
                                <div class="col-12">                               
                                    <div class="scrollable">                              
                                    <form action="funciones.php" method="POST" enctype="multipart/form-data"> 
                                    <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>>                                                
                                            <table>
                                            <thead class="thead-dark">
                                            <?php while ($perfil = pg_fetch_array($res333)):;?> 
                                            <tr class="titulotabla">
                                                        <th colspan="2" >Titulo</th>
                                                        <td colspan="10"><?php echo $perfil['titulo'];?></td>
                                                 </tr>
                                                    <tr>
                                                        <th  class="text-center">ID  </th>                                                        
                                                        <th class="text-center">CodSIS  </th>
                                                        <th class="text-center"> Nombre  </th>
                                                        <th class="text-center">Apellido P. </th>
                                                        <th class="text-center">Apellido M. </th>                                                       
                                                        <th class="text-center">Estado </th>
                                                                                                             
                                                </thead>                                               
                                                <tbody>
                                                    <tr>                                                                                                                              
                                                        <td class="text-center"><?php echo $perfil['id'];   ?></td>  
                                                        <td class="text-center"><?php echo $perfil['cod_sis']; ?></td> 
                                                        <td class="text-center"><?php echo $perfil['nombre']; ?></td>    
                                                        <td class="text-center"><?php echo $perfil['apellidop']; ?></td>     
                                                        <td class="text-center"><?php echo $perfil['apellidom'];?></td>                                                         
                                                        <td class="text-center"><?php echo $perfil['ciclo'];?></td>   
                                                                                                                                                                                             
                                                    </tr>
                                                    <?php endwhile;?> 
                                                </tbody>                                                    
                                            </table>                                                                                                                             
                                    </div>   
                                    <div class="form-group row">
                                        <div class="col-9"></div>
                                            <div class="col-3">                                                     
                                                <button type="submit" class="btn btn-danger" value="<?php  echo $perfil['cod_sis'];?>" name="export-excel">EXPORTAR</button>                                                                                                   
                                            </div>
                                         </div>            
                                    </div>                                 
                                </div> 
                            </div>                          
                        </form>        

                    <div Class="wrapper" id="editardatos" class="container-fluid" style="display:none;" class="answer_list">
                        <form action="funciones.php" method="POST">
                            <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>> 
                            <input type="hidden" class="list-group-item" name="rol" id="rol" value=<?php echo $_SESSION['rol']?>>
                            <h2 class="temporal" col>EL FORMULARIO ESTA EN CONSTRUCCION CON 90% DE AVANCE, SOLO FALTA LA DEBIDA ADAPTACION DE UNA TABLA EN LA BD ESPECIFICA PARA LOS ADMIN, DIRECTOR DE CARRERA Y SUPREMO</h2>  

                            <div class="form-group row">
                                <label for="formGroupExampleInput" class="col-4 col-form-label">NOMBRE</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="nombre" name="nombre"  value="<?php  echo $linea[3];?>">
                                </div>
                            </div>
        
                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-4 col-form-label">APELLIDO PATERNO</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="apellidop" name="apellidop"  value="<?php  echo $linea[4];?>">		
                                </div>
                            </div>
                
                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-4 col-form-label">APELLIDO MATERNO</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="apellidom" name="apellidom"  value="<?php  echo $linea[5];?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-4 col-form-label">FECHA NACIMIENTO</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="fec_nac" name="fec_nac" value="<?php  echo $linea[6];?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="formGroupExampleInput" class="col-4 col-form-label">TELEFONO</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="telefono" name="telefono" value="<?php  echo $linea[7];?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="formGroupExampleInput" class="col-4 col-form-label">EMAIL</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="correo" name="correo" value="<?php  echo $linea[8];?>">
                                </div>
                            </div>
        
                            <div class="form-group row">
                                <div class="col-3">
                                    <button type="submit" class="btn btn-danger" name="env_est">ENVIAR</button>
                                </div>
                            </div>
                        </form>
                    </div>

                               <div Class="wrapper" id="editarArea" class="container-fluid" style="display:none;" class="answer_list">
                        <form action="funciones.php" method="POST">
                            <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>> 
                            
                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-3 col-form-label">AREA</label>
                                <div class="col-9">
                                    <select class="js-example-basic-single" style="width: 100%" name="area">
                                        <?php while ($row = pg_fetch_array($area)):;?>                                                        
                                            <option value=<?php echo $row['id_area'];?> onclick="tocarLista('<?php echo $row['nom_area'];?>', ' <?php $valor=$row['descripcion_area'];
                                                if($valor === '\'')
                                                {

                                                }
                                                else
                                                {
                                                    echo $valor;
                                                }?>')" ><?php echo $row['nom_area'];?>
                                            </option>                                                        
                                        <?php endwhile; ?>      
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-4 col-form-label">NOMBRE</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="nombreAreaEdit" name="nombre"  placeholder="<?php  ?>">		
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-4 col-form-label">DESCRIPCION</label>
                                <div class="col-8">
                                    <textarea class="form-control" rows="3" name="descripcion" id="descripAreaEdit"></textarea>	
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-3">
                                    <button type="submit" class="btn btn-danger" name="editarAreas">ACTUALIZAR</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div Class="wrapper" id="editarSubArea" class="container-fluid" style="display:none;" class="answer_list">
                        <form action="funciones.php" method="POST">
                            <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>> 
                            
                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-3 col-form-label">SUB-AREA</label>
                                <div class="col-9">
                                    <select class="js-example-basic-single" style="width: 100%"  name="subarea">
                                        <?php while ($row = pg_fetch_array($subarea)):;?>                                                         
                                            <option value=<?php echo $row['id_sub'];?> onclick="tocarListaSub('<?php echo $row['nom_sub'];?>', ' <?php $valor=$row['descripcion'];
                                                if($valor === '\'')
                                                {
                                                 
                                               }
                                                else
                                                {
                                                    echo $valor;
                                                }?>')" ><?php echo $row['nom_sub'];?>
                                            </option>                                                         
                                        <?php endwhile; ?>      
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-4 col-form-label">NOMBRE</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="nomSub" name="nombreSubArea"  placeholder="<?php  ?>">		
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-4 col-form-label">DESCRIPCION</label>
                                    <div class="col-8">
                                        <textarea class="form-control" rows="3" id="desSub" name="desSubArea"  placeholder="<?php  ?>">		</textarea>
                                    </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-3">
                                    <button type="submit" class="btn btn-danger" name="editarSubAreas">ACTUALIZAR</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div Class="wrapper" id="eliminarSubArea" class="container-fluid" style="display:none;" class="answer_list">
                        <form action="funciones.php" method="POST">
                            <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>> 
                            
                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-3 col-form-label">SUB-AREA</label>
                                <div class="col-9">
                                    <select class="js-example-basic-single" style="width: 100%"  name="subarea">
                                        <?php while ($row = pg_fetch_array($subareaEliminacion)):;?>                                                         
                                            <option value=<?php echo $row['id_sub'];?>><?php echo $row['nom_sub'];?></option>                                                          
                                        <?php endwhile; ?>      
                                    </select>
                                </div>
                            </div>
                            
                                </br>
                            <div class="form-group row">
                                <div class="col-3">
                                    <button type="submit" class="btn btn-danger" name="eliminarSubAreas">ELIMINAR</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div Class="wrapper" id="eliminarArea" class="container-fluid" style="display:none;" class="answer_list">
                        <form action="funciones.php" method="POST">
                            <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>> 
                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-3 col-form-label">AREA</label>
                                <div class="col-9">
                                    <select class="js-example-basic-single" style="width: 100%"  name="area">
                                        <?php while ($row = pg_fetch_array($areaEliminacion)):;?>                                                         
                                            <option value=<?php echo $row['id_area'];?>><?php echo $row['nom_area'];?></option>                                                          
                                        <?php endwhile; ?>      
                                    </select>
                                </div>
                            </div>
                            
                                </br>
                            <div class="form-group row">
                                <div class="col-3">
                                    <button type="submit" class="btn btn-danger" name="eliminarAreas">ELIMINAR</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div Class="wrapper" id="area" class="container-fluid" style="display:none;" class="answer_list">
                        <form action="funciones.php" method="POST" enctype="multipart/form-data">
                        <div class="form-group row">
                                   <label for="exampleFormControlSelect1" class="col-4 col-form-label">LISTA AREAS</label>
                                      <div class="col-8">
                                           <select class="js-example-basic-single" style="width: 100%" >
                                              <?php                                                          
                                                 while ($row = pg_fetch_array($areaS)):;?>                                                         
                                                <option><?php echo $row[1];?></option>                                                         
                                              <?php endwhile;
                                               ?>      
                                             </select>
                                          </div>
                                       </div>
                            <div class="form-group row">
                                <label for="Area" class="col-4 col-form-label">AREA</label>
                                <div class="col-8">
                                    <input type="address" class="form-control" id="area" name="area" placeholder="Area">
                                </div>
                            </div>   
                                                      
                            <div class="form-group row">
                                <label for="formGroupExampleInput" class="col-4 col-form-label">Descripción</label>
                                <div class="col-8">
                                    <textarea class="form-control" rows="3" name="descripcion" id="comment"></textarea>
                                </div>
                            </div>              

                            <div class="form-group row">
                                <div class="col-3">
                                    <button type="submit" name="botonAreas" class="btn btn-danger">REGISTRAR</button>
                                </div>
                            </div>
                        </form>        
                    </div> 

                    <div Class="wrapper" id="subarea" class="container-fluid" style="display:none;" class="answer_list">
                        <form action="funciones.php" method="POST" enctype="multipart/form-data">
                            <div class="form-group row">
                                <label for="exampleFormControlSelect1" class="col-3 col-form-label">AREA</label>
                                <div class="col-9">
                                    <select class="js-example-basic-single" style="width: 100%" name="area">
                                        <?php while ($row = pg_fetch_array($subarealist)):;?>                                                         
                                            <option value=<?php echo $row['id_area'];?>><?php echo $row['nom_area'];?></option>                                                         
                                        <?php endwhile; ?>      
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="Sub Area" class="col-4 col-form-label">SUB AREA</label>
                                <div class="col-8">
                                    <input type="address" class="form-control" id="subarea" name="subAreaReg" placeholder="Sub Area">
                                </div>
                            </div>   
                                                
                            <div class="form-group row">
                                <label for="formGroupExampleInput" class="col-4 col-form-label">Descripción</label>
                                <div class="col-8">
                                    <textarea class="form-control" rows="3" name="subAreaDescripcionReg" id="comment"></textarea>
                                </div>
                            </div>          

                            <div class="form-group row">
                                <div class="col-3">
                                    <button type="submit" name="botonSubAreas" class="btn btn-danger">REGISTRAR</button>
                                </div>
                            </div>  
                        </form>    
                    </div>   

                  

                    <div class="wrappertable" id="estudiantesSistema" class="container-fluid" style="display:none;" class="answer_list">
                    <form action="funciones.php" method="POST" enctype="multipart/form-data"  style="border:2px solid blue;"> 
                    <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>>                                                      
                        <h2 class="tituloespacio">ESTUDIANTES REGISTRADOS EN EL SISTEMA</h2>  
                        <div class="form-group row">
                            <label  class="col-2 col-form-label">BUSCAR</label>
                            <div class="col-sm-10">
                                <select class="js-example-basic-single" style="width: 100%" name="ciValidar">>
                                    <?php while ($row = pg_fetch_array( $recordsDS)):;?>                                                         
                                    <option value="<?php echo $row[1];?>">
                                            <?php 
                                                echo $row[1]."&nbsp;&nbsp;&nbsp;" ; echo $row[2]."&nbsp;&nbsp;&nbsp;"; echo $row[3]."&nbsp;&nbsp;&nbsp;";
                                                echo $row[4]."&nbsp;&nbsp;&nbsp;";  echo $row[5]."&nbsp;&nbsp;&nbsp;";
                                            ?>
                                        </option>                                                         
                                    <?php endwhile; ?>														
                                </select>
                            </div>
                        </div>
                       <!--   <div class="form-group row">
                            <div class="col-7"></div>
                            <div class="col-5">                    
                                <button  type="submit" class="btn btn-danger float-right"  name="remover1">REMOVER</button>                                     
                            </div>
                        </div> -->  
                        </form>
                        <div class="col-7"></div>
                        <form action="funciones.php" method="POST" enctype="multipart/form-data"  style="border:2px solid black;"> 
                            <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>>                                                                  
                            <div class="row">                               
                            <div class="col-12"></div>                                     
                                <div class="col-12">                               
                                    <div class="scrollable">                              
                                                                               
                                            <table>
                                            <thead class="thead-dark">                                                                                 
                                                    <tr>
                                                        <th  class="text-center">ID  </th>   
                                                        <th  class="text-center">CI  </th>                                                      
                                                        <th class="text-center">CodSIS  </th>
                                                        <th class="text-center"> Nombre  </th>
                                                        <th class="text-center">Apellido P. </th>
                                                        <th class="text-center">Apellido M. </th>   
                                                        <th class="text-center">Fecha Nacimiento </th>   
                                                  <!--      <th class="text-center"></th>     -->                                   
                                                </thead>                                                
                                                <tbody>
                                                <?php while ($perfil = pg_fetch_array( $records1DS)):;?>     
                                                    <tr>                                                                                                                              
                                                        <td class="text-center"> <?php echo $perfil['id'];   ?></td> 
                                                        <td class="text-center"><?php echo $perfil['ci']; ?></td>  
                                                        <td class="text-center"><?php echo $perfil['cod_sis']; ?></td> 
                                                        <td class="text-center"><?php echo $perfil['nombre']; ?></td>    
                                                        <td class="text-center"><?php echo $perfil['apellidop']; ?></td>     
                                                        <td class="text-center"><?php echo $perfil['apellidom'];?></td>
                                                        <td class="text-center"><?php echo $perfil['fechanaci'];?></td>                                                          
                                                        <!--    <td class="text-center">   
                                                    <a href="#" class="btn">Aceptar</a>|  
                                                        <a href="#" class="btn">Remover</a>
                                                        <input type="checkbox" name="chk[]" value="<?php  echo $perfil['ci'];?>">
                                                    </td>  -->
                                                        <?php endwhile;?>                                                                                                                                                  
                                                    </tr>
                                                </tbody>                                                    
                                            </table>                                                                                                                             
                                    </div>   
                                   <!--   <div class="form-group row">
                                        <div class="col-9"></div>
                                            <div class="col-3">                                                     
                                            <button type="submit" class="btn btn-danger float-right  " value="<?php  echo $perfil['ci'];?>" name="removerSupre">REMOVER</button>                                                                                                         
                                            </div>
                                         </div>  -->          
                                    </div>                                 
                                </div> 
                            </div>                          
                        </form>                          

                        <div class="wrappertable" id="docentesSistema" class="container-fluid" style="display:none;" class="answer_list">
                        <form action="funciones.php" method="POST" enctype="multipart/form-data"  style="border:2px solid blue;"> 
                       <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>>                                                    
                        <h2 class="tituloespacio">DOCENTES REGISTRADOS EN EL SISTEMA</h2>  
                        <div class="form-group row">
                            <label  class="col-2 col-form-label">BUSCAR</label>
                            <div class="col-sm-10">
                                <select class="js-example-basic-single" style="width: 100%"name="ciValidar">
                                    <?php while ($row = pg_fetch_array($recorD)):;?>                                                         
                                    <option value="<?php echo $row[1];?>">
                                            <?php 
                                                echo $row[1]."&nbsp;&nbsp;&nbsp;" ; echo $row[2]."&nbsp;&nbsp;&nbsp;"; echo $row[3]."&nbsp;&nbsp;&nbsp;";
                                                echo $row[4]."&nbsp;&nbsp;&nbsp;";  echo $row[5]."&nbsp;&nbsp;&nbsp;";
                                            ?>
                                        </option>                                                         
                                    <?php endwhile; ?>														
                                </select>
                            </div>
                        </div>
                        <!-- <div class="form-group row">
                            <div class="col-7"></div>
                            <div class="col-5">                    
                                <button  type="submit" class="btn btn-danger float-right"  name="remover1">REMOVER</button>                                     
                            </div>
                        </div>-->  
                       
                        </form>
                        <div class="col-7"></div>
                        <form action="funciones.php" method="POST" enctype="multipart/form-data"  style="border:2px solid black;"> 
                       <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>>                                                            
                            <div class="row">                               
                            <div class="col-12"></div>                                     
                                <div class="col-12">                               
                                    <div class="scrollable">                              
                                                                               
                                            <table>
                                            <thead class="thead-dark">                                                                                 
                                                    <tr>
                                                        <th  class="text-center">ID  </th>  
                                                        <th  class="text-center">CI</th>                                                          
                                                        <th class="text-center">CodSIS  </th>
                                                        <th class="text-center"> Nombre  </th>
                                                        <th class="text-center">Apellido P. </th>
                                                        <th class="text-center">Apellido M. </th>   
                                                        <th class="text-center">Fecha Nacimiento </th>                                           
                                                </thead>                                                
                                                <tbody>
                                                <?php while ($perfil1 = pg_fetch_array($recorD1)):;?>     
                                                    <tr>                                                                                                                              
                                                        <td class="text-center"> <?php echo $perfil1['id'];   ?></td>  
                                                        <td class="text-center"> <?php echo $perfil1['ci'];   ?></td>  
                                                        <td class="text-center"><?php echo $perfil1['cod_sis']; ?></td> 
                                                        <td class="text-center"><?php echo $perfil1['nombre']; ?></td>    
                                                        <td class="text-center"><?php echo $perfil1['apellidop']; ?></td>     
                                                        <td class="text-center"><?php echo $perfil1['apellidom'];?></td>
                                                        <td class="text-center"><?php echo $perfil1['fechanaci'];?></td>                                                          
                                                        <td class="text-center">   
                                                    <!--<a href="#" class="btn">Aceptar</a>|  
                                                        <a href="#" class="btn">Remover</a>-->
                                                        <input type="checkbox" name="chk[]" value="<?php  echo $perfil1['ci'];?>">
                                                    </td>                                         
                                                        <?php endwhile;?>                                                                                                                                                  
                                                    </tr>
                                                </tbody>                                                    
                                            </table>                                                                                                                             
                                    </div>   
                                  <!--    <div class="form-group row">
                                        <div class="col-9"></div>
                                            <div class="col-3">                                                     
                                            <button type="submit" class="btn btn-danger float-right  " value="<?php  echo $perfil1['ci'];?>" name="removerSupre">REMOVER</button>                                                                                        
                                            </div>
                                         </div> -->             
                                    </div>                                 
                                </div> 
                            </div>                          
                        </form>

                          <div class="wrappertable" id="adminSistema" class="container-fluid" style="display:none;" class="answer_list">
                          <form action="funciones.php" method="POST" enctype="multipart/form-data"  style="border:2px solid blue;"> 
                       <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>>                                                       
                        <h2 class="tituloespacio">LISTA ADMINISTRADORES REGISTRADOS EN EL SISTEMA</h2>  
                        <div class="form-group row">
                            <label  class="col-2 col-form-label">BUSCAR</label>
                            <div class="col-sm-10">
                                <select class="js-example-basic-single" style="width: 100%" name="ciValidar">
                                    <?php while ($row = pg_fetch_array( $recorAd1)):;?>                                                         
                                    <option value="<?php echo $row[1];?>">
                                            <?php 
                                                echo $row[1]."&nbsp;&nbsp;&nbsp;" ; echo $row[2]."&nbsp;&nbsp;&nbsp;"; echo $row[3]."&nbsp;&nbsp;&nbsp;";
                                                echo $row[4]."&nbsp;&nbsp;&nbsp;";  echo $row[5]."&nbsp;&nbsp;&nbsp;";
                                            ?>
                                        </option>                                                         
                                    <?php endwhile; ?>														
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-7"></div>
                            <div class="col-5">                    
                                <button  type="submit" class="btn btn-danger float-right"  name="remover1">REMOVER</button>                                     
                            </div>
                        </div>
                        </form>
                        <div class="col-7"></div>
                        <form action="funciones.php" method="POST" enctype="multipart/form-data"  style="border:2px solid black;"> 
                       <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>>                                                          
                            <div class="row">                               
                            <div class="col-12"></div>                                     
                                <div class="col-12">                               
                                    <div class="scrollable">                              
                                        <form>                                        
                                            <table>
                                            <thead class="thead-dark">                                                                                 
                                                    <tr>
                                                        <th  class="text-center">ID  </th>  
                                                        <th  class="text-center">CI</th>                                                          
                                                        <th class="text-center">CodSIS  </th>
                                                        <th class="text-center"> Nombre  </th>
                                                        <th class="text-center">Apellido P. </th>
                                                        <th class="text-center">Apellido M. </th>   
                                                        <th class="text-center">Fecha Nacimiento </th>                                           
                                                </thead>                                                
                                                <tbody>
                                                <?php while ($perfil1 = pg_fetch_array($recorAd2)):;?>     
                                                    <tr>                                                                                                                              
                                                        <td class="text-center"> <?php echo $perfil1['id'];   ?></td>  
                                                        <td class="text-center"> <?php echo $perfil1['ci'];   ?></td>  
                                                        <td class="text-center"><?php echo $perfil1['cod_sis']; ?></td> 
                                                        <td class="text-center"><?php echo $perfil1['nombre']; ?></td>    
                                                        <td class="text-center"><?php echo $perfil1['apellidop']; ?></td>     
                                                        <td class="text-center"><?php echo $perfil1['apellidom'];?></td>
                                                        <td class="text-center"><?php echo $perfil1['fechanaci'];?></td>                                                          
                                                        <td class="text-center">   
                                                    <!--<a href="#" class="btn">Aceptar</a>|  
                                                        <a href="#" class="btn">Remover</a>-->
                                                        <input type="checkbox" name="chk[]" value="<?php  echo $perfil1['ci'];?>">
                                                    </td>                                         
                                                        <?php endwhile;?>                                                                                                                                                  
                                                    </tr>
                                                </tbody>                                                    
                                            </table>                                                                                                                             
                                    </div>   
                                    <div class="form-group row">
                                        <div class="col-9"></div>
                                            <div class="col-3">                                                     
                                            <button type="submit" class="btn btn-danger float-right  " value="<?php  echo $perfil1['ci'];?>" name="removerSuper">REMOVER</button>                                                                                        
                                            </div>
                                         </div>            
                                    </div>                                 
                                </div> 
                            </div>                          
                        </form>

                        <div class="wrappertable" id="exportar" class="container-fluid" style="display:none;" class="answer_list">                    
                        <form action="funciones.php" method="POST" enctype="multipart/form-data"> 
                        <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>> 
                        <input type="hidden" class="list-group-item" name="rol" id="rol" value=<?php echo $_SESSION['rol']?>> 
                        
                        <h2 class="tituloespacio">EXPORTAR DATOS A EXCEL</h2>                         
                            <div class="row">                               
                            <div class="col-12"></div>                                     
                                <div class="col-12">                               
                                    <div class="scrollable">                                                                
                                            <table>
                                                <thead class="thead-dark">
                                                    <tr>
                                                        <th class="text-center">ID</th>       
                                                        <th class="text-center">Nombre Tabla</th>   
                                                        <th class="text-center"></th>                                                                                                      
                                                    </tr>
                                                </thead>
                                                <?php while ( $tabla =pg_fetch_array($res1191)):;?>  
                                                <tbody>
                                                    <tr>    
                                                        <td class="text-center"> <?php  echo $tabla['id'];?></td>                                                                                                                             
                                                        <td class="text-center"> <?php  echo $tabla['table_name'];?></td>  
                                                        <td class="text-center"> </td>                                                                                                                                                                                                                              
                                                        <td class="text-center">                                                   
                                                            <button type="submit" class="btn btn-outline-danger btn-sm " value="<?php  echo $tabla['table_name'];?>" name="excelSupremo">EXPORTAR</button>                                                                                                   
                                                         </td>                                                                                                                                  
                                                         </tr><?php endwhile;?>     
                                                </tbody>                                                                            
                                            </table>                                                                                                                               
                                    </div>                                   
                                </div>                                
                                </div> 
                                </div>                                                         
                        </form>  
                  

                      <div class="wrappertable" id="bitacoraA" class="container-fluid" style="display:none;" class="answer_list">                    
                        <form action="funciones.php" method="POST" enctype="multipart/form-data"> 
                        <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>> 
                        <input type="hidden" class="list-group-item" name="rol" id="rol" value=<?php echo $_SESSION['rol']?>> 
                        <h2 class="tituloespacio">BITACORA A</h2>                         
                            <div class="row">                               
                            <div class="col-12"></div>                                     
                                <div class="col-12">                               
                                    <div class="scrollable">                                                                
                                            <table>
                                                <thead class="thead-dark">
                                                    <tr>
                                                    <th class="text-center">Nombre de Tabla</th>       
                                                        <th class="text-center">Operacion</th>   
                                                        <th class="text-center">Valores Anteriores</th>        
                                                        <th class="text-center">Nuevos Valores</th>        
                                                        <th class="text-center"></th>                                                                                                      
                                                    </tr>
                                                </thead>
                                                <?php while ( $tabla =pg_fetch_array($zaga)):;?>  
                                                <tbody>
                                                    <tr>    
                                                        <td class="text-center"> <?php  echo $tabla['table_name'];?></td>                                                                                                                             
                                                        <td class="text-center"> <?php  echo $tabla['operation'];?></td>  
                                                        <td class="text-center"> <?php  echo $tabla['before_value'];?></td>  
                                                        <td class="text-center"> <?php  echo $tabla['after_value'];?></td>  
                                                        <td class="text-center"> </td>                                                                                                                                                                                                                              
                                                        <td class="text-center">                                                   
                                                         </td>                                                                                                                                  
                                                         </tr><?php endwhile;?>   
                                                </tbody>                                                                            
                                            </table>                                                                                                                               
                                    </div>                                   
                                </div>                                
                                </div> 
                                </div>                                                       
                        </form>  


                        <div class="wrappertable" id="bitacoraB" class="container-fluid" style="display:none;" class="answer_list">                    
                        <form action="funciones.php" method="POST" enctype="multipart/form-data"> 
                        <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>> 
                        <input type="hidden" class="list-group-item" name="rol" id="rol" value=<?php echo $_SESSION['rol']?>>  
                        <h2 class="tituloespacio">BITACORA B</h2>                         
                            <div class="row">                               
                            <div class="col-12"></div>                                     
                                <div class="col-12">                               
                                    <div class="scrollable">                                                                
                                            <table>
                                            <table>
                                                <thead class="thead-dark">
                                                    <tr>
                                                    <th class="text-center">Nombre de Tabla</th>       
                                                        <th class="text-center">Operacion</th>   
                                                        <th class="text-center">Valores Anteriores</th>        
                                                        <th class="text-center">Nuevos Valores</th>        
                                                        <th class="text-center"></th>                                                                                                      
                                                    </tr>
                                                </thead>
                                                <?php while ( $tabla1 =pg_fetch_array($kand)):;?>  
                                                <tbody>
                                                    <tr>    
                                                        <td class="text-center"> <?php  echo $tabla1['table_name'];?></td>                                                                                                                             
                                                        <td class="text-center"> <?php  echo $tabla1['operation'];?></td>  
                                                        <td class="text-center"> <?php  echo $tabla1['before_value'];?></td>  
                                                        <td class="text-center"> <?php  echo $tabla1['after_value'];?></td>  
                                                        <td class="text-center"> </td>                                                                                                                                                                                                                              
                                                        <td class="text-center">                                                   
                                                         </td>                                                                                                                                  
                                                         </tr><?php endwhile;?>   
                                                </tbody>                                                                            
                                            </table>                                                                                                                               
                                    </div>                                   
                                </div>                                
                                </div> 
                                </div>                                                         
                        </form> 
                        <div class="wrappertable" id="listaAreaA" class="container-fluid" style="display:none;" class="answer_list">                    
                        <form action="funciones.php" method="POST" enctype="multipart/form-data"> 
                        <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>> 
                        <input type="hidden" class="list-group-item" name="rol" id="rol" value=<?php echo $_SESSION['rol']?>>
                        <h2 class="tituloespacio">LISTA AREA</h2>                         
                            <div class="row">                               
                            <div class="col-12"></div>                                     
                                <div class="col-12">                               
                                    <div class="scrollable">                                                                
                                            <table>
                                                <thead class="thead-dark">
                                                    <tr>
                                                        <th class="text-center">ID</th>       
                                                        <th class="text-center">Nombre Tabla</th>   
                                                        <th class="text-center"></th>                                                                                                      
                                                    </tr>
                                                </thead>
                                                <?php while ( $tabla =pg_fetch_array($res1191)):;?>  
                                                <tbody>
                                                    <tr>    
                                                        <td class="text-center"> <?php  echo $tabla['id'];?></td>                                                                                                                             
                                                        <td class="text-center"> <?php  echo $tabla['table_name'];?></td>  
                                                        <td class="text-center"> </td>                                                                                                                                                                                                                              
                                                        <td class="text-center">                                                   
                                                            <button type="submit" class="btn btn-outline-danger btn-sm " value="<?php  echo $tabla['table_name'];?>" name="excelSupremo">EXPORTAR</button>                                                                                                   
                                                         </td>                                                                                                                                  
                                                         </tr><?php endwhile;?>     
                                                </tbody>                                                                            
                                            </table>                                                                                                                               
                                    </div>                                   
                                </div>                                
                                </div> 
                                </div>                                                         
                        </form>  

                         <div class="wrappertable" id="listaAreaB" class="container-fluid" style="display:none;" class="answer_list">                    
                        <form action="funciones.php" method="POST" enctype="multipart/form-data"> 
                        <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>> 
                        <input type="hidden" class="list-group-item" name="rol" id="rol" value=<?php echo $_SESSION['rol']?>>
                        <h2 class="tituloespacio">LISTA SUB-AREA</h2>                         
                            <div class="row">                               
                            <div class="col-12"></div>                                     
                                <div class="col-12">                               
                                    <div class="scrollable">                                                                
                                            <table>
                                                <thead class="thead-dark">
                                                    <tr>
                                                        <th class="text-center">ID</th>       
                                                        <th class="text-center">Nombre Tabla</th>   
                                                        <th class="text-center"></th>                                                                                                      
                                                    </tr>
                                                </thead>
                                                <?php while ( $tabla =pg_fetch_array($res1191)):;?>  
                                                <tbody>
                                                    <tr>    
                                                        <td class="text-center"> <?php  echo $tabla['id'];?></td>                                                                                                                             
                                                        <td class="text-center"> <?php  echo $tabla['table_name'];?></td>  
                                                        <td class="text-center"> </td>                                                                                                                                                                                                                              
                                                        <td class="text-center">                                                   
                                                            <button type="submit" class="btn btn-outline-danger btn-sm " value="<?php  echo $tabla['table_name'];?>" name="excelSupremo">EXPORTAR</button>                                                                                                   
                                                         </td>                                                                                                                                  
                                                         </tr><?php endwhile;?>     
                                                </tbody>                                                                            
                                            </table>                                                                                                                               
                                    </div>                                   
                                </div>                                
                                </div> 
                                </div>                                                         
                        </form>  
                        <div class="wrappertable"  id="verificarUsuario" class="container-fluid" style="display:none;" class="list">
                    <div class="form-group row">
                                <div class="col-11">
                                  <h2>ESTUDIANTES ESPERANDO APROBACION PARA INGRESAR AL SISTEMA</h2> 
                                  
                                </div>
                                <button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#AyudaBloque">?</button>
                            </div>
                   
                       <form action="funciones.php" method="POST" enctype="multipart/form-data" style="border:2px solid blue;"> 
                       <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>> 
                            <input type="hidden" class="list-group-item" name="rol" id="rol" value=<?php echo $_SESSION['rol']?>>  
                        <div class="form-group row" >
                            <label  class="col-2 col-form-label">BUSCAR</label>
                            <div class="col-sm-10">
                                <select class="js-example-basic-single" style="width: 100%" name="ciValidar">>
                                    <?php while ($row = pg_fetch_array($records1)):;?>                                                         
                                        <option value="<?php echo $row[1];?>">
                                            <?php 
                                                echo $row[1]."&nbsp;&nbsp;&nbsp;" ; echo $row[2]."&nbsp;&nbsp;&nbsp;"; echo $row[3]."&nbsp;&nbsp;&nbsp;";
                                                echo $row[4]."&nbsp;&nbsp;&nbsp;";  echo $row[5]."&nbsp;&nbsp;&nbsp;"; 
                                            ?>
                                        </option>                                                         
                                    <?php endwhile; ?>														
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-7"></div>
                            <div class="col-5">                                                     
                                <!-- <button type="submit" class="btn btn-info "  name="env_est">ACEPTAR</button>
                                <button type="submit" class="btn btn-danger float-right  "  name="remover">REMOVER</button> -->                                                      
                                <button type="submit" class="btn btn-info "   name="validar1">ACEPTAR</button>
                                <button  type="submit" class="btn btn-danger float-right"  name="remover1">REMOVER</button>                                     
                            </div>
                        </div>
                        </form>
                        <div class="col-7"></div>
                        <form action="funciones.php" method="POST" enctype="multipart/form-data" style="border:2px solid black;"> 
                       <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>> 
                            <input type="hidden" class="list-group-item" name="rol" id="rol" value=<?php echo $_SESSION['rol']?>>  
                            <div class="row">                                          
                                <div class="col-12">
                                    <div class="scrollable">
                                     
                                        <form>
                                            <table>
                                                <thead class="thead-dark">
                                                    <tr>
                                                        <th class="text-center">ID  </th>
                                                        <th class="text-center">CI  </th>
                                                        <th class="text-center">CodSIS  </th>
                                                        <th class="text-center">Nombre  </th>
                                                        <th class="text-center">Apellido P. </th>
                                                        <th class="text-center">Apellido M. </th>                                                                                                                                                                                                          
                                                        <th class="text-center">SELECCION</th>                                                                       
                                                    </tr>
                                                </thead>
                                                <?php while ($persona = pg_fetch_array($records)):;?>  
                                                <tbody>
                                                    <tr>                                                                                                                              
                                                        <td class="text-center">   <?php  echo $persona['id'];?></td>  
                                                        <td class="text-center">   <?php  echo $persona['ci'];?></td> 
                                                        <td class="text-center">   <?php  echo $persona['cod_sis'];?></td> 
                                                        <td class="text-center">   <?php  echo $persona['nombre'];?></td> 
                                                        <td class="text-center">   <?php  echo $persona['apellidop'];?></td> 
                                                        <td class="text-center">   <?php  echo $persona['apellidom'];?></td>                                                                                                                                            
                                                        <td class="text-center">   
                                                            <input type="checkbox" name="chk[]" value="<?php  echo $persona['ci'];?>">
                                                        </td>                                                                                                                                                           
                                                    </tr><?php endwhile;?>
                                                </tbody>                                                                            
                                            </table>                                                                                                                               
                                    </div> 

                                    <div class="form-group row">
                                        <div class="col-7"></div>
                                            <div class="col-5">                                                     
                                                <button type="submit" class="btn btn-info "  value="validar" name="validarS">ACEPTAR</button>
                                                <button type="submit" class="btn btn-danger float-right  " value="<?php  echo $persona['ci'];?>" name="removerS">REMOVER</button>                                                      
                                            </div>
                                         </div>            
                                    </div> 
                                </div> 
                            </div>  
                        </form> 

                           <div class="wrappertable"  id="verificarDocente" class="container-fluid" style="display:none;" class="list">
                    <div class="form-group row">
                                <div class="col-11">
                                <h2>DOCENTE ESPERANDO APROBACION PARA INGRESAR AL SISTEMA</h2>
                                  
                                </div>
                                <button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#AyudaBloque">?</button>
                            </div>
                    <form action="funciones.php" method="POST" enctype="multipart/form-data" style="border:0.5px solid blue;"> 
                      <input type="hidden" class="list-group-item" name="ci" id="ci" value=<?php echo $_SESSION['ci']?>> 
                        <div class="form-group row">
                            <label  class="col-2 col-form-label">BUSCAR</label>
                            <div class="col-sm-10">
                                <select class="js-example-basic-single" style="width: 100%" name="ciValidar">>
                                    <?php while ($row = pg_fetch_array($recordsDocente1)):;?>                                                         
                                        <option value="<?php echo $row[1];?>">
                                            <?php 
                                                echo $row[1]."&nbsp;&nbsp;&nbsp;" ; echo $row[2]."&nbsp;&nbsp;&nbsp;"; echo $row[3]."&nbsp;&nbsp;&nbsp;";
                                                echo $row[4]."&nbsp;&nbsp;&nbsp;";  echo $row[5]."&nbsp;&nbsp;&nbsp;"; 
                                            ?>
                                        </option>                                                         
                                    <?php endwhile; ?>														
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-7"></div>
                            <div class="col-5">                                                     
                                <!-- <button type="submit" class="btn btn-info "  name="env_est">ACEPTAR</button>
                                <button type="submit" class="btn btn-danger float-right  "  name="remover">REMOVER</button> -->                                                      
                                <button type="submit" class="btn btn-info "   name="validar1">ACEPTAR</button>
                                <button  type="submit" class="btn btn-danger float-right"  name="remover1">REMOVER</button>                                     
                            </div>
                        </div>
                        </form>
                        <div class="col-7"></div>
                        <div class="col-7"></div>
                        <form style="border:0.5px solid black;">
                            <div class="row">
                                <div class="col-12">
                                    <div class="scrollable">
                                      
                                        <table>
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th class="text-center">ID  </th>
                                                    <th class="text-center">CI  </th>
                                                    <th class="text-center">CodSIS  </th>
                                                    <th class="text-center">Nombre  </th>
                                                    <th class="text-center">Apellido P. </th>
                                                    <th class="text-center">Apellido M. </th>
                                                    <th class="text-center">Fecha N. </th>
                                                    <th class="text-center">SELECCION </th>
                                                </tr>
                                            </thead>
                                            <?php while ($persona = pg_fetch_assoc($recordsDocente)):;?>  
                                            <tbody>
                                                <tr>                                                        
                                                    <td class="text-center">   <?php  echo $persona['id'];?></td>  
                                                    <td class="text-center">   <?php  echo $persona['ci'];?></td> 
                                                    <td class="text-center">   <?php  echo $persona['cod_sis'];?></td> 
                                                    <td class="text-center">   <?php  echo $persona['nombre'];?></td> 
                                                    <td class="text-center">   <?php  echo $persona['apellidop'];?></td> 
                                                    <td class="text-center">   <?php  echo $persona['apellidom'];?></td>   
                                                    <td class="text-center">   <?php  echo $persona['fechanaci'];?></td>                                                                    
                                                    <td class="text-center">   
                                                    <!--<a href="#" class="btn">Aceptar</a>|  
                                                        <a href="#" class="btn">Remover</a>-->
                                                        <input type="checkbox" name="chk[]" value="<?php  echo $persona['ci'];?>">
                                                    </td>                                                                                                
                                                </tr><?php endwhile;?>
                                            </tbody>
                                        </table>                                                                                             
                                    </div> 

                                    <div class="form-group row">
                                            <div class="col-7"></div>
                                                <div class="col-5">                                                     
                                                    <button type="submit" class="btn btn-info "  value="validar" name="validarS">ACEPTAR</button>
                                                    <button type="submit" class="btn btn-danger float-right  " value="<?php  echo $persona['ci'];?>" name="removerS">REMOVER</button>                                                      
                                                </div>
                                            </div>            
                                        </div>
                                    </div>  
                                </div> 
                            </div>
                        </form>            

                      
                      

                      
                        
                        
                        <div class="modal fade" id="AyudaActualizar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                 <div class="modal-dialog modal-dialog-centered" role="document">
                                                     <div class="modal-content">
                                                       <div class="modal-header">
                                                           <h5 class="modal-title" id="exampleModalLongTitle">AYUDA</h5>
                                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                              </button>
                                                        </div>
                                                    <div class="modal-body">
                                                           Los docentes seran visibles una vez ellos COMPLETEN SU REGISTROS en sus
                                                           cuentas propias.
                                                     </div>
                                                          
                                                      </div>
                                                </div>
                                            </div>  
                       
                                                                                        <!-- Modal -->
                                                                                        <div class="modal fade" id="AyudaBloque" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                 <div class="modal-dialog modal-dialog-centered" role="document">
                                                     <div class="modal-content">
                                                       <div class="modal-header">
                                                           <h5 class="modal-title" id="exampleModalLongTitle">AYUDA</h5>
                                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                              </button>
                                                        </div>
                                                    <div class="modal-body">
                                                          BUSCAR un usuario para ACEPTAR O RECHAZAR se realiza en su mismo bloque de color azul.<br>
                                                          La SELECCION de un usuario para ACEPTAR O RECHAZAR se realiza en su mismo bloque de color negro.

                                                     </div>
                                                          
                                                      </div>
                                                </div>
                    

                    <div class="wrapper" id="cambioTema" class="container-fluid" style="display:none;" class="answer_list"> </div> 

                    <div class="wrapper" id="solicitud" class="container-fluid" style="display:none;" class="answer_list"> </div>

                    <div Class="wrapper" id="eliminarSubArea" class="container-fluid" style="display:none;" class="answer_list"></div>

                    <div Class="wrapper" id="eliminarArea" class="container-fluid" style="display:none;" class="answer_list"></div>
    
                    <div Class="wrapper" id="subarea" class="container-fluid" style="display:none;" class="answer_list"></div>
    
                
    
                    <div class="wrapper" id="enviarreg" class="container-fluid" style="display:none;" class="answer_list"></div>			
    
                    <div class="wrapper" id="registroperfilDivEstu" class="container-fluid" style="display:none;" class="answer_list"></div> 

                    <div class="wrappertable"  id="cambioTemaAdmin" class="container-fluid" style="display:none;" class="list"></div>

                    <div class="wrapper" id="solicitud" class="container-fluid" style="display:none;" class="answer_list"> </div>

                    <div class="wrapper" id="abandono" class="container-fluid" style="display:none;" class="answer_list"></div>

                      <div class="wrappertable"  id="listaEstudiantesT" class="container-fluid" style="display:none;" class="list"></div>

                      <div class="wrapper" id="cambioTutor" class="container-fluid" style="display:none;" class="answer_list"></div>

                      <div class="wrappertable"  id="cambiotemaEstu" class="container-fluid" style="display:none;" class="list"></div>

                       <div class="wrappertable" id="estudiantesSistema" class="container-fluid" style="display:none;" class="answer_list"></div>

                       <div class="wrappertable" id="docentesSistema" class="container-fluid" style="display:none;" class="answer_list"></div>

                        <div class="wrapper" id="CambioTutorEstu" class="container-fluid" style="display:none;" class="answer_list"></div>
                </div>				
            
            <div class="footer esconder">
                <p>Copyright © 2018 EKEKO S.A. All rights reserved.</p>
           </div>
        </div>
    </body>
</html>

