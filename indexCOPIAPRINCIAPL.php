<?php
include "funciones.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title></title>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"> 
	<link rel="stylesheet" href="css/zerogrid.css" type="text/css" media="screen">
	<link rel="stylesheet" href="css/responsive.css" type="text/css" media="screen"> 	
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css" >	
    <script src="js/cufon-yui.js" type="text/javascript"></script>
    <script src="js/cufon-replace.js" type="text/javascript"></script>
    <script src="js/NewsGoth_400.font.js" type="text/javascript"></script>                  
</head>

<body>
  
	<div class="extra">
        <!--==============================header=================================-->
        <header>
            <div class="container-fluid">
                <nav class="navbar">
                    <a class="navbar-brand"  target="_blank" rel="noopener noreferrer" href="http://www.umss.edu.bo/">
                    <?php echo '<img height="600 width="600" src="data:image/png;base64,'.$data1[0].' "> ';?>
                        <!--<img src="http://casa.fcyt.umss.edu.bo/images/shares/Logo1.png" width="400" height="450">-->
                    </a>
                    <a class="esconder" target="_blank" rel="noopener noreferrer" href="http://fcyt.umss.edu.bo/">
                    <?php echo '<img height="120" width="140" src="data:image/png;base64,'.$data2[0].' "> ';?>
                          <!--<img class="esconder" src="http://www.umss.edu.bo/wp-content/uploads/2018/01/logo-fcyt.png"     width="130" height="150">-->
                    </a>
                </nav>
            </div>
        </header>
        <!--==============================content================================-->
        <div class="container titulo2">                   
            <div class="row titulo">                         
                <div class="col-8 esconder">
                    <h1>BIENVENIDOS AL SISTEMA DE</h1>                        
                </div> 
                <div class="col-8">
                    <h1>REGISTRO DE PERFILES DE PROYECTOS</h1>                        
                </div> 
                <div class="col-8">
                    <h1>FINALES DE GRADO</h1>                        
                </div>                         
            </div> 
        </div>
    <div class="container-fluid mensaje">                   
        <div class="row" >
            <div class="col-lg-4 esconder">
                <h4>
                    El Sistema de Registro de Perfiles de Proyecto de Grado ofrece una 
                    administracion de la información a cabalidad, rapida y correcta.
                    Informacion como el registro de perfil, tutor, asignacion, estado de revisión, 
                    conclusion y defensa del trabajo etc....
                </h4>  
                <a target="_blank" rel="noopener noreferrer" href="http://www.fcyt.umss.edu.bo/trabajodegrado/catalogo.php" class="btn btn-secondary" role="button" aria-disabled="true">Tesis</a>   
                <a target="_blank" rel="noopener noreferrer" href="http://www.cs.umss.edu.bo/rep_tipo_tesiss.jsp" class="btn btn-secondary" role="button" aria-disabled="true">Perfil</a>                     
            </div>  
            <div class="col-lg-4"></div>
            <div class="col-lg-4">

                <div>                                                         
                    <form class="col-6" action="login.php" method="post">
                        <div class="form-group">
                            <input type="text" name="login" class="form-control" placeholder="USUARIO">                       
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" placeholder="CONTRASEÑA">                    
                        </div>
                        <div class="form-group row">
                              <div class="col-sm-10">
                                     <button type="submit" class="btn btn-dark btn-lg">LOGIN</button>
                            </div>
                        </div>                                 
                    </form>    
                    
                    <div class="container">                   
                        <div class="row">   
                        <div class="col-lg-4"></div>                       
                            <div class="col-8">   							                                                 
                            <a href="registro.php" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Nuevo Usuario</a>
                            </div>
                        </div>
                    </div>
                </div>                                                                               
            </div>
        </div>
        <div class="footer">
             <p>Copyright © 2018 EKEKO S.A. All rights reserved.</p>
        </div>
    </div>
</body>
</html>

