<?php
include "funciones.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Formulario</title>	
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"> 
	<link rel="stylesheet" href="css/zerogrid.css" type="text/css" media="screen">
	<link rel="stylesheet" href="css/responsive.css" type="text/css" media="screen"> 	
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css" >     
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="js/datepicker-es.js" type="text/javascript"></script> 
	<style>	
	form{background-color: rgba(0,0,0,0.4);border-radius: 3px;color: #999;font-size: 0.8em;padding: 20px;margin: 0 auto;width: 345px;}
	input, textarea{border: 0;outline: none;width: 300px;}
	.field{border: solid 1px #ccc;padding: 10px;}
	.field:focus{border-color: #000;}
	.center-content{text-align: center;}
    .box p{margin: 5px;padding: 10px;font-weight: bold;color: #fff;font-size:20px;}		
	</style>
</head>
<body  class="extra3">
	<div class="container tituloF box">
		<form action="funciones.php" method="POST">
			<div class="form-group">
				<p>Codigo SIS: De 7 a 9 Digitos.</p>
					<input type="text" placeholder="CodSIS" required="Formulario.php" name="CodSIS" class="field"> <br/>
			</div>
			<div class="form-group">
				<p>C.I.: De 7 a 9 Digitos</p>
					<input type="text" placeholder="C.I." name="ci"  required="Formulario.php" class="field"> <br/>
				</div>
			<div class="form-group">
				<p>Fecha de Nacimiento:</p>
					<input type="datepicker" id="datepicker" class="form-control" name="nombre5" placeholder="dia/mes/año">
			</div>	
				<button type="submit" name="botonrecu" class="btn btn-primary">Enviar</button>
		</form>		
			<div class="form-group espacio">
      			<a class="navbar-brand" href="index.php">
					<button type="submit" class="btn btn-info">INICIO</button>
        		</a>    
  			</div>
		</div>

		</div>
		<div class="footer">
             <p>Copyright © 2018 EKEKO S.A. All rights reserved.</p>
        </div> 
    <script>  
   $( function() {
    $( "#datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true
    });
  } );
     </script> 
</body>

</html>
