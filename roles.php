<?php      
session_start();
if(!isset($_SESSION["temporal"]))
{
header("location:index.php");
print_r($_SESSION);
}
include ("funciones.php");     
?>    
<!DOCTYPE html>
<html lang="en">
<head>
    <title></title>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"> 
	<link rel="stylesheet" href="css/zerogrid.css" type="text/css" media="screen">
	<link rel="stylesheet" href="css/responsive.css" type="text/css" media="screen"> 	
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css" >	
    <script src="js/cufon-yui.js" type="text/javascript"></script>
    <script src="js/cufon-replace.js" type="text/javascript"></script>
    <script src="js/NewsGoth_400.font.js" type="text/javascript"></script>  
    <script src="js/menu.js" type="text/javascript"></script>                
</head>

<body>
	<div class="extra1">
        <!--==============================header=================================-->       
        <div class="extra">
            <!--==============================header=================================-->
            <header>
                <div class="container-fluid">
                    <nav class="navbar">
                        <a class="navbar-brand"  target="_blank" rel="noopener noreferrer" href="http://www.umss.edu.bo/">
                        <?php echo '<img height="600 width="600" src="data:image/png;base64,'.$data1[0].' "> ';?>
                            <!--<img src="http://casa.fcyt.umss.edu.bo/images/shares/Logo1.png" width="400" height="450">-->
                        </a>
                        <a class="esconder" target="_blank" rel="noopener noreferrer" href="http://fcyt.umss.edu.bo/">
                        <?php echo '<img height="120" width="140" src="data:image/png;base64,'.$data2[0].' "> ';?>
                            <!--<img class="esconder" src="http://www.umss.edu.bo/wp-content/uploads/2018/01/logo-fcyt.png"     width="130" height="150">-->
                        </a>
                    </nav>
                </div>
            </header>

            <header>
                <div class="row">
                    <div class="col-lg-3"><p class="h3 text-muted"></p> </div>
                        <div class="col-lg-3"></div>
                                <div class="col-lg-3"></div>								
                        <div class="col-lg-3"> 
                            <a class="navbar-brand" href="logout.php"><input type="submit" name="enviar" value="CERRAR SESION" class="btn btn-success btn-lg"></a>
                        </div>
                </div>                             
            </header> 

            <div class="container">
                <div class="row titulo">                         
                    <div class="col-8">
                        <h2>ELEGIR SU ROL</h2>                        
                    </div> 
                    <div class="col-8">
                        <h2>PARA INGRESAR AL SISTEMA</h2>                        
                    </div> 
                    <div class="col-8">
                        <h1></h1>                        
                    </div>                         
                </div> 
            </div>     
        
            <div class="wrapper10">
                <form role="form" method="post" class="espacio">                     
                    <div class="form-group">
                        <div class="form-check form-check-inline">
                            <label class="container1">DOCENTE
                                <input type="radio" checked="checked" name="radio" value="1004">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-check form-check-inline">
                            <label class="container1">DIRECTOR CARRERA
                                <input type="radio" checked="checked" name="radio" value="1002">
                                <span class="checkmark"></span>
                            </label>
                        </div>  
                    </div>   
                        
                    <div class="form-group" id="rolSupremo">
                        <div class="form-check form-check-inline">
                            <label class="container1">SUPREMO
                                <input type="radio" checked="checked" name="radio" value="1001">
                                <span class="checkmark"></span>
                            </label>
                        </div>  
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-10">
                            <button type="submit" name="roles" class="btn btn-dark btn-lg">LOGIN</button>
                        </div>
                    </div>                                               
                </form>                  
            </div>      
        </div> 
        <div class="footer">
             <p>Copyright © 2018 EKEKO S.A. All rights reserved.</p>
        </div>
    </div>   
</body>

<script type='text/javascript'>
    var xyz;
    xyz = <?php echo $value; ?>;
    var t="3";
        if (xyz==t) {
            document.getElementById("rolSupremo").style.visibility = "visible";        
        } else {
            document.getElementById("rolSupremo").style.visibility = "hidden";      
        }  
</script>
</html>
